import {Component, OnInit} from '@angular/core';
import {environment} from '../environments/environment';
import {ActivatedRoute, Router} from '@angular/router';
import {UserIdleService} from 'angular-user-idle';
import {ToastrManager} from 'ng6-toastr-notifications';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = environment.applicationDisplayName;
  public loading = false;

  constructor(public router: Router, route: ActivatedRoute,
              public toastr: ToastrManager, private userIdle: UserIdleService) {
  }

  ngOnInit() {
    // Start watching for user inactivity.
    this.userIdle.startWatching();

    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe(count => {
      if (count === 1) {
        this.toastr.warningToastr('Session is about to Expire....', 'Alert!');
      }
    });

    // Start watch when time is up.
    this.userIdle.onTimeout().subscribe(() => {
      this.userIdle.stopTimer();
      this.userIdle.stopWatching();
      this.router.navigate(['/login']);
      localStorage.setItem('isLoggedin', 'false');
    });
  }

  loader(boolValue) {
    this.userIdleWatch();
    this.loading = boolValue;
  }

  onActivate() {
    window.scroll(0, 0);
  }

  userIdleWatch() {
    // Start watching for user inactivity.
    this.userIdle.stopTimer();
  }
}
