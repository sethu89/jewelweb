export class PicklistContainer {
  public value: string;
  public key: string;

  constructor(key: string, value: string) {
    this.value = value;
    this.key = key;
  }
}
