export class User {
    id: number;
    firstName: string;
    lastName: string;
    userName: string;
    pwd: string;
    designation: string;
    active: boolean;
    roles: string;
    deskId: number;
    constructor(id: number, firstName: string, lastName: string, userName: string, pwd: string, designation: string,
        active: boolean, roles: string, deskId: number) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.pwd = pwd;
        this.designation = designation;
        this.active = active;
        this.roles = roles;
        this.deskId = deskId;
    }
}
