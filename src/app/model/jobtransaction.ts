export class JobTransaction {
    id: number;
    productId: number;
    userId: number;
    deskId: number;
    inTime: string;
    outTime: string;
    jobType: string;
    status: string;
    execute: '';

    constructor( execute: '', id: number, productId: number, userId: number, deskId: number, inTime: string, outTime: string,
                 jobType: string, status: string, ) {

        this.id = id;
        this.productId = productId;
        this.userId = userId;
        this.deskId = deskId;
        this.inTime = inTime;
        this.outTime = outTime;
        this.jobType = jobType;
        this.status = status;
    }
}
