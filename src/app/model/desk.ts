export class DeskForm {
    id: number;
    name: string;
    description: string;
    status: string;
    deskType: string

    constructor(id: number, name: string, description: string, status: string, deskType: string) {

        this.id = id;
        this.name = name;
        this.description = description;
        this.status = status;
        this.deskType = deskType;
    }
}
