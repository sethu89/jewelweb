export class MasterData {
    id: number;
    category: string;
    code: string;
    description: string;
    activeDt: string;
    expireDt: string;
    notes: string;
    displaySeq: number;
    constructor(id: number, category: string, code: string, description: string, activeDt: string, expireDt: string,
        notes: string, displaySeq: number) {
        this.id = id;
        this.category = category;
        this.code = code;
        this.description = description;
        this.activeDt = activeDt;
        this.expireDt = expireDt;
        this.notes = notes;
        this.displaySeq = displaySeq;
    }
}
