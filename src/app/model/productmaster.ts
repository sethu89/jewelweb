export class ProductMasterForm {
    id: number;
    name: string;
    status: string;
    barCodeChar: string;
    imagePath: string;
    execute: '';
    img: String;
    file: File;

    constructor( execute: '', id: number, name: string, status: string, barCodeChar: string, imagePath: string,
                 img: String, file: File,) {

        this.id = id;
        this.name = name;
        this.status = status;
        this.barCodeChar = barCodeChar;
        this.imagePath = imagePath;
        this.img = img;
        this.file = file;
    }
}
