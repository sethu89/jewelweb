export class MasterCategory {
    id: number;
    category: string;
    picklist: string;
   
    constructor(id: number, category: string, picklist: string) {
        this.id = id;
        this.category = category;
        this.picklist = picklist;
    }
}
