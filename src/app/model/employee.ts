export class EmployeeForm {
    id: number;
    empId: string;
    firstName: string;
    lastName: string;
    email: string;
    designation: string;
    department: string;
    dateOfJoining: string;
    experienceInYr: number;
    userId: string;
    active: string;
  contactNumber: string;
  aadharNumber: string;
  dateOfBirth: string;
  maritalStatus: string;
  education: string;


  constructor(id: number, empId: string, firstName: string, lastName: string, email: string, designation: string, department: string,
        dateOfJoining: string, experienceInYr: number, userId: string, active: string, contactNumber: string,
              aadharNumber: string,  dateOfBirth: string,  maritalStatus: string, education: string) {

        this.id = id;
        this.empId = empId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.designation = designation;
        this.department = department;
        this.dateOfJoining = dateOfJoining;
        this.experienceInYr = experienceInYr;
        this.userId = userId;
        this.active = active;
    this.contactNumber = contactNumber;
    this.aadharNumber = aadharNumber;
    this.dateOfBirth = dateOfBirth;
    this.maritalStatus = maritalStatus;
    this.education = education;

  }
}
