export class Codes {
  id: number;
  category: String;
  code: String;
  description: String;
  activeDt: String;
  expireDt: String;
  notes: String;

  constructor(id: number, category: String, code: String, description: String, activeDt: String, expireDt: String, notes: String) {
    this.id = id;
    this.category = category;
    this.code = code;
    this.description = description;
    this.activeDt = activeDt;
    this.expireDt = expireDt;
    this.notes = notes;
  }
}
