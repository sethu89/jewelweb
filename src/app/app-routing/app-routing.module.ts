import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {LayoutModule} from '../layout/layout.module';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', redirectTo: 'layout', pathMatch: 'full' },
      { path: 'layout', component: LayoutModule },
    ])
  ],
  declarations: [],
  exports: [ RouterModule]
})
export class AppRoutingModule { }
