import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SERVER} from '../server';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FactoryService} from '../shared/services';
import {USER_LOGIN} from '../shared/services/endpoints';
import {AppComponent} from '../app.component';
import {environment} from '../../environments/environment';
import {DomSanitizer} from '@angular/platform-browser';
import {PicklistService} from 'src/app/shared/services';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Globals} from '../shared/global';



@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    deskForm: FormGroup;
    showDeskDropDown = false;
    showForm = true;
    public userName: string;
    public userId: Number;
    public pass: string;
    public password: string;

    public masterPass: string;
    public showReset = false;
    public showError = false;
    public errMsg: string;
    public applicationDisplayName: string;
    public miniName: string;
    public imgFolder: string;
    public backgroundImg: string;
    designationTypeValue = new Map<String, String>();

    constructor(public router: Router, route: ActivatedRoute, private http: HttpClient, private fs: FactoryService,
                public appComponent: AppComponent, private sanitizer: DomSanitizer,
                private _sanitizer: DomSanitizer, private pick: PicklistService, private fb: FormBuilder,
                public globals: Globals) {

        this.showReset = false;
        this.showReset = route.snapshot.queryParams['showReset'];
        this.applicationDisplayName = environment.applicationDisplayName;
        this.miniName = environment.miniName;
        this.imgFolder = environment.imgFolder;
        this.backgroundImg = environment.backgroundImg;

        const roles = localStorage.getItem('roles');
        if (roles != null && roles.indexOf('ADMIN', 0) > -1) {
            this.showReset = true;
        }
    }

    ngOnInit() {
        this.createSearchCriteriaForm(this.fb);
    }

    createSearchCriteriaForm(fb: FormBuilder) {
        this.deskForm = fb.group({
            deskId: [null],
        });
    }


    fetchPickListValues() {
        this.pick.fetchPickListValues().subscribe(dataReturned => {
            const loc = <Map<String, Map<String, String>>>dataReturned;
            this.designationTypeValue = loc.get('DEPARTMENT');
        });
    }

    onLoggedin() {
        // this.showError = true;
        this.appComponent.loader(true);
        if (this.userName && this.pass) {
            const url = USER_LOGIN;
            this.http.get<Array<String>>(url, {
                withCredentials: true,
                headers: new HttpHeaders().set('Content-Type', 'application/json')
                    .set('Authorization', 'Basic ' + btoa(this.userName + ':' + this.pass)),
                observe: 'response'
            })
                .subscribe(resp => {
                        localStorage.setItem('X-QQ-Auth-token', resp.headers.get('X-QQ-Auth-token'));
                        this.fetchPickListValues();
                        this.userId = Number(resp['body']);
                        localStorage.setItem('userId', String(this.userId));
                        localStorage.setItem('isLoggedin', 'true');
                        localStorage.setItem('loggedUser', this.userName);
                        localStorage.setItem('roles', resp.headers.get('X-QQ-Auth-roles'));
                        this.router.navigate(['/layout/welcome']);
                        this.appComponent.loader(false);
                    },
                    err => {
                        this.showError = true;
                        this.errMsg = 'Unable to login.Please check the username and password';
                        this.router.navigate(['/login']);
                        localStorage.setItem('isLoggedin', 'false');
                        this.appComponent.loader(false);
                    }
                );
        } else {
            if (!this.userName) {
                this.showError = true;
                this.errMsg = 'Please enter the user name';
                this.appComponent.loader(false);
            } else if (!this.pass) {
                this.showError = true;
                this.errMsg = 'Please enter the password';
                this.appComponent.loader(false);
            }
        }
    }

    resetPassword() {
        const headers = this.fs.getHttpHeaders();
        const url = SERVER + '/spasvcs/access/reset';
        const postData = '{"userName":"' + this.userName + '", "password":"' + this.password + '", "masterPass":"' + this.masterPass + '"}';
        this.http.post(url, postData, {
            withCredentials: true,
            headers: headers
        })
            .subscribe(resp => {
                },
                err => {
                }
            );
    }

    public sanitizeImage() {
        return this._sanitizer.bypassSecurityTrustStyle(`url(${this.backgroundImg})`);
    }
}
