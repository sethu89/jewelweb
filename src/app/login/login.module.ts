import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LoginRoutingModule} from './login-routing.module';
import {LoginComponent} from './login.component';
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import {LayoutComponent} from '../layout/layout.component';
import { UserService } from 'src/app/shared/services/user.service';
import { Globals } from '../shared/global';


@NgModule({
    imports: [CommonModule,
      LoginRoutingModule,
      ReactiveFormsModule,
      FormsModule,
      NgxLoadingModule.forRoot({}),
    ],
  providers: [LayoutComponent, UserService, Globals],
    declarations: [LoginComponent]
})
export class LoginModule {}
