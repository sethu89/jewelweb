import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import {browser} from 'protractor';
import {CommonModule} from '@angular/common';
import {LoginRoutingModule} from './login-routing.module';
import {FormsModule} from '@angular/forms';
import {NgxLoadingModule} from 'ngx-loading';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {FactoryService} from '../shared/services';
import {LayoutComponent} from '../layout/layout.component';
import {AppComponent} from '../app.component';

fdescribe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
const page = LoginComponent;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [CommonModule,
        LoginRoutingModule,
        FormsModule,
        NgxLoadingModule.forRoot({}),
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([]),
      ],
      providers: [FactoryService, LayoutComponent, AppComponent]
    })
    .compileComponents();
     fixture = TestBed.createComponent(LoginComponent);
     component = fixture.componentInstance;
     fixture.detectChanges();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it ('should display an error message to the user if they didnt provided User Name', () => {
    // component.pass = 'ag';
    component.onLoggedin();
    expect(component.errMsg).toBe('Please enter the user name');
  });

  it ('should display an error message to the user if they didnt provided Password', () => {
    component.userId = 'ag';
    component.onLoggedin();
    expect(component.errMsg).toBe('Please enter the password');
  });

  // it ('should display an error message to the user if they didnt provided Password', () => {
  //   component.userId = 'wrong';
  //   component.pass = 'wrong';
  //   component.onLoggedin();
  //   expect(component.errMsg).toBe('Unable to login.Please check the username and password');
  // });
});
