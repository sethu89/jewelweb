import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from '../../../shared/services/user.service';
import { AppComponent } from '../../../app.component';
import { User } from 'src/app/model/user';
import { PicklistService } from 'src/app/shared/services';
import { Globals } from '../../../shared/global';
import {ToastrManager} from 'ng6-toastr-notifications';
import {DeskService} from '../../../shared/services/desk.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('userId') userIdEdited: number;
  userEditForm: FormGroup;
  title: string;
  message: string;
  activeTypeValue = new Map<String, String>();
  rolesData = [];
  rolesMap = new Map<String, String>();
  deskMap = new Map<String, String>();
  showPassword = true;

  constructor(private fb: FormBuilder, private uService: UserService, public appComponent: AppComponent, private pick: PicklistService,
    public globals: Globals,  public toastr: ToastrManager, public deskService: DeskService) {
    this.createEditForm(this.fb);
  }

  ngOnInit() {
    this.fetchPickListValues();
    this.createNewUser();
    this.retrieveRoles();
    this.retrieveDesks();
    if (this.userIdEdited > 0) {
      this.retrieve();
      this.showPassword = false;
    }
  }

  retrieveDesks() {
    this.deskService.fetchAllDesk('').subscribe(data => {
      if (data.length > 0) {
        data.forEach(desk  => {
          this.deskMap.set(desk['id'], desk['name']);
        });
      }
    });
  }

  fetchPickListValues() {
    this.pick.fetchPickListValues().subscribe(dataReturned => {
      const loc = <Map<String, Map<String, String>>>dataReturned;
      this.activeTypeValue = loc.get('ACTIVE_STATUS');
    });
  }
  createNewUser() {
    this.title = this.globals.createTitle;
    this.createEditForm(this.fb);
  }

  createEditForm(fb: FormBuilder) {
    this.userEditForm = fb.group({
      firstName: [null],
      lastName: [null],
      userName: [null],
      pwd: [null],
      active: '1',
      roles: [null],
      deskId: [null],
      id: [null]
    });
  }
  displayUserRetrievedForm(fb: FormBuilder, retrieved: User) {
    this.userEditForm = fb.group({
      firstName: retrieved.firstName,
      lastName: retrieved.lastName,
      userName: retrieved.userName,
      pwd: retrieved.pwd,
      active: retrieved.active,
      roles: retrieved.roles,
      deskId: retrieved.deskId,
      id: retrieved.id
    });
  }
  retrieve() {
    this.title = this.globals.editTitle;
    this.retrieveUser();
  }

  retrieveUser() {
    this.appComponent.loader(true);
    this.uService.fetchUserById(this.userIdEdited).subscribe(data => {
      this.displayUserRetrievedForm(this.fb, data);
      this.showPassword = false;
    },
      err => {
        this.toastr.warningToastr(err['statusText'], 'Alert!', {position: 'bottom-right'});
      });
    this.appComponent.loader(false);
  }
  retrieveRoles() {
    this.appComponent.loader(true);
    this.uService.fetchAllRoles().subscribe(data => {
      this.rolesData = data.rolesList;
      this.rolesMap = data.RolesMapName;
    },
      err => {
        this.toastr.warningToastr(err['statusText'], 'Alert!', {position: 'bottom-right'});
      });
    this.appComponent.loader(false);
  }

  duplicateCheck(postData) {
    this.appComponent.loader(true);
    const searchData = 'userName=' + encodeURIComponent(postData.userName);
    let validData = false;
    this.message = '';
    this.uService.searchUser(searchData).subscribe(data => {
      if (data && data.length > 0) {
        if (postData.id && postData.id === data[0].id) {
          validData = true;
        } else {
          this.toastr.infoToastr(postData.userName + this.globals.userNameExist, 'Info!', {position: 'bottom-right'});
        }
      } else {
        validData = true;
      }

      if (validData) {
        this.submituserForm(postData);
      }
    });
    this.appComponent.loader(false);
  }

  submituserForm(postData) {
    this.appComponent.loader(true);
    if (this.userIdEdited > 0) {
      postData.id = this.userIdEdited;
    }
    this.uService.saveUser(postData).subscribe(data => {
      if (this.userIdEdited > 0) {
        this.toastr.successToastr(this.globals.userUpdateMsg, 'Success!', {position: 'bottom-right'});
      } else {
        this.toastr.successToastr(this.globals.userAddedMsg, 'Success!', {position: 'bottom-right'});
      }
      this.userIdEdited = data['id'];
      this.retrieveUser();
    },
      err => {
        this.toastr.warningToastr(err['statusText'], 'Alert!', {position: 'bottom-right'});
      });
    this.appComponent.loader(false);
  }

}

