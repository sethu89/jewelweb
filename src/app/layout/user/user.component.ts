import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  showSearch: boolean;
  userId: number;
  constructor() {
    this.showSearch = true;

  }

  ngOnInit() {
  }
  hideSearch(userId: number) {
    this.userId = userId;
    this.showSearch = false;
  }

}
