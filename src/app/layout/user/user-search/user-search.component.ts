import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from '../../../shared/services/user.service';
import { AppComponent } from '../../../app.component';
import { Globals } from '../../../shared/global';
import {ToastrManager} from 'ng6-toastr-notifications';
import {DeskService} from '../../../shared/services/desk.service';

@Component({
  selector: 'app-user-search',
  templateUrl: './user-search.component.html',
  styleUrls: ['./user-search.component.scss']
})
export class UserSearchComponent implements OnInit {
  @Output() itemFound = new EventEmitter<number>();
  userSearchForm: FormGroup;
  showSearchResult = false;
  searchResult = [];
  message: string;
  deskMap = new Map<number, String>();


  constructor(private fb: FormBuilder, private uService: UserService,  public appComponent: AppComponent,
    public toastr: ToastrManager, public globals: Globals, public deskService: DeskService) {
    this.createSearchForm(this.fb);
   }

  ngOnInit() {
    this.fetchDeskList();
  }

  fetchDeskList() {
    this.deskService.fetchAllDesk('').subscribe(data => {
      if (data.length > 0) {
        data.forEach(desk  => {
          this.deskMap.set(desk['id'], desk['name']);
        });
      }
    });
  }

  createSearchForm(fb: FormBuilder) {
    this.userSearchForm = fb.group({
      userName: [null],
    });
  }

  submituserForm(postData) {
    this.appComponent.loader(true);
    let searchData = '';
    for (const field of Object.keys(postData)) {
       if (postData[field]) {
        searchData = searchData + field + '=' + encodeURIComponent(postData[field]) + '&';
       }
    }
    this.getDataToService(searchData);
    this.appComponent.loader(false);
  }

  getDataToService(postData) {
    this.appComponent.loader(true);
    this.uService.searchUser(postData).subscribe(data => {
      if (data) {
        this.showSearchResult = true;
        this.searchResult = data;
        this.toastr.successToastr(data.length + this.globals.userRetrieved, 'Success!');
      } else {
        this.showSearchResult = false;
        this.toastr.infoToastr(this.globals.noUserRetrieved, 'Info!');
      }
      this.appComponent.loader(false);
    });
  }

  onAddUser() {
    this.itemFound.emit(0);
  }
  onViewUser(userId) {
    this.itemFound.emit(userId);
  }

}
