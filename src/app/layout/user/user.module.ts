import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { UserSearchComponent } from './user-search/user-search.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UserEditComponent } from './user-edit/user-edit.component';
import {UserService} from '../../shared/services/user.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import {Globals} from '../../shared/global';
import {FormsModule} from '@angular/forms';
import {DeskService} from '../../shared/services/desk.service';

@NgModule({
  declarations: [
    UserComponent,
    UserSearchComponent,
    UserEditComponent,
  ],
  imports: [
    CommonModule, ReactiveFormsModule, UserRoutingModule, AngularMultiSelectModule, FormsModule
  ],

  entryComponents: [UserEditComponent],
  bootstrap: [UserEditComponent],
  providers: [UserEditComponent, UserService, Globals, DeskService],
})
export class UserModule {
}
