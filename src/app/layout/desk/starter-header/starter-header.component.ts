import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';


@Component({
  selector: 'app-starter-header',
  templateUrl: './starter-header.component.html',
  styleUrls: ['./starter-header.component.scss']
})
export class StarterHeaderComponent implements OnInit {
  userName: String;
  applicationDisplayName: String;
  miniName: String;
  img;

  constructor( private http: HttpClient) {
    this.userName =  localStorage.getItem('loggedUser');
    this.applicationDisplayName = environment.applicationDisplayName;
    this.miniName = environment.miniName;
    this.getFolder().subscribe( data => {
      this.img = data;
    });
   }

  getFolder(): Observable<string> {
    // const folderPath = 'assets/img/' + this.userName.toLowerCase() + '.jpg';
    const folderPath = 'assets/img/' + this.userName + '.jpg';
    return this.http
      .get(`${folderPath}`, { observe: 'response', responseType: 'blob' })
      .pipe(
        map(response => {
          return folderPath;
        }),
        catchError(error => {
          return of('assets/img/user.png');
        })
      );
  }

  ngOnInit() {
  }

}
