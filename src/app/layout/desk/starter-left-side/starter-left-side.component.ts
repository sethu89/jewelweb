import { Component, OnInit, Type } from '@angular/core';
import { FactoryService } from '../../../shared/services';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../../../shared/global';

@Component({
  selector: 'app-starter-left-side',
  templateUrl: './starter-left-side.component.html',
  styleUrls: ['./starter-left-side.component.scss']
})
export class StarterLeftSideComponent implements OnInit {
  userName: String;
  isAdmin = false;
  sideMenuMap: Map<string, String[]> = new Map<string, String[]>();
  roles;
  img;

  constructor(private fs: FactoryService,  private http: HttpClient, public globals: Globals) {
    this.userName = localStorage.getItem('loggedUser');
    this.isAdmin = this.fs.isUserAdmin();
    this.roles = this.fs.getUserRoles().split('|');
    this.getFolder().subscribe( data => {
      this.img = data;
    });

    // All Master
    this.sideMenuMap.set('AllMaster', ['ROLE_ADMIN', 'ROLE_MANAGER']);
    // Posvcs
    this.sideMenuMap.set('Codes', ['ROLE_ADMIN', 'ROLE_MANAGER']);
    this.sideMenuMap.set('ProductMaster', ['ROLE_ADMIN', 'ROLE_MANAGER']);
    this.sideMenuMap.set('user', ['ROLE_ADMIN', 'ROLE_MANAGER']);
    this.sideMenuMap.set('Desk', ['ROLE_ADMIN', 'ROLE_MANAGER']);
    this.sideMenuMap.set('JobTransaction', ['ROLE_ADMIN', 'ROLE_MANAGER']);
    this.sideMenuMap.set('user', ['ROLE_ADMIN']);


    // Quality
    this.sideMenuMap.set('QualityReport', ['ROLE_ADMIN', 'ROLE_MANAGER']);


    // Inventory Module
    this.sideMenuMap.set('inventoryReport', ['ROLE_ADMIN', 'ROLE_MANAGER']);
    this.sideMenuMap.set('InventoryMaster', ['ROLE_ADMIN', 'ROLE_MANAGER']);
    this.sideMenuMap.set('ProductMaster', ['ROLE_ADMIN', 'ROLE_MANAGER']);
    this.sideMenuMap.set('BarCode', ['ROLE_ADMIN', 'ROLE_MANAGER']);
    this.sideMenuMap.set('ProductDetail', ['ROLE_ADMIN', 'ROLE_MANAGER']);
    this.sideMenuMap.set('Reports', ['ROLE_ADMIN', 'ROLE_MANAGER']);

  }

  getFolder(): Observable<string> {
    const folderPath = 'assets/img/' + this.userName.toLowerCase() + '.jpg';
    return this.http
      .get(`${folderPath}`, { observe: 'response', responseType: 'blob' })
      .pipe(
        map(response => {
          return folderPath;
        }),
        catchError(error => {
          return of('assets/img/user.png');
        })
      );
  }

  ngOnInit() {
  }

  checkRole(sideMenu) {
    const approvedRoles = this.sideMenuMap.get(sideMenu);
    const found = approvedRoles.some(r => this.roles.indexOf(r) >= 0);
    if (approvedRoles.some(r => this.roles.indexOf(r) >= 0)) {
      return true;
    }
  }

}
