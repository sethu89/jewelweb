import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StarterContentComponent} from './starter-content.component';

const routes: Routes = [
    {
        path: '',
        component: StarterContentComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WelcomeRoutingModule {}
