import { Component, OnInit } from '@angular/core';
import {DASHBOARD_DATA} from '../../../shared/services/endpoints';
import {FactoryService} from '../../../shared/services';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

declare var AdminLTE: any;

@Component({
  selector: 'app-starter-content',
  templateUrl: './starter-content.component.html',
  styleUrls: ['./starter-content.component.scss']
})
export class StarterContentComponent implements OnInit {

  poTotal: number;
  partTotal: number;
  parentPartCount: number;
  applicationDisplayName: String;
  // weeklyRunningeData: any = weeklyRunningData;
  chart;
  showDashBoard;

  constructor(private fs: FactoryService, private http: HttpClient) {
    this.applicationDisplayName = environment.applicationDisplayName;
    if (this.fs.isUserAdmin() || this.fs.isUserManager()) {
      this.showDashBoard = true;
    } else {
      this.showDashBoard = false;
    }

  }

  ngOnInit() {
    // Update the AdminLTE layouts
    this.getDashboardData();
    AdminLTE.init();
  }

  getDashboardData() {
    const url = DASHBOARD_DATA;
    const headers = this.fs.getHttpHeaders();
    this.http.get<any>(url, {
      withCredentials: true,
      headers: headers
    })
      .subscribe(data => {
        this.poTotal = data.poCount;
        this.partTotal = data.partCount;
        this.parentPartCount = data.parentPartCount;
      });
  }

}
