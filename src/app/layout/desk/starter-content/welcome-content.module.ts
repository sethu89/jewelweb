import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WelcomeRoutingModule} from './welcome-routing.module';
import {StarterContentComponent} from './starter-content.component';

@NgModule({
  declarations: [StarterContentComponent],
  imports: [
    CommonModule,
    WelcomeRoutingModule
  ]
})
export class WelcomeContentModule { }
