import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { FactoryService } from '../../../shared/services';
import { RESET_PASSWORD } from 'src/app/shared/services/endpoints';
import { Globals } from '../../../shared/global';
import {ToastrManager} from 'ng6-toastr-notifications';


@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('resetpasswordId') resetpasswordIdEdited: number;
  resetPasswordForm: FormGroup;
  return = true;
  userName: String;
  invalid: boolean;
  password = false;
  title: String;
  noMatch: false;
  message: string;
  showSuccessAlert: boolean;
  showWarningAlert: boolean;


  constructor(private formBuilder: FormBuilder, private http: HttpClient, private fs: FactoryService,
    public globals: Globals,  public toastr: ToastrManager) {
    this.userName = localStorage.getItem('loggedUser');
  }

  ngOnInit() {

    this.resetPasswordForm = this.formBuilder.group({
      confirmpwd: ['', [Validators.required]],
      newPwd: ['', [Validators.required ]],
      currentPwd: ['', [Validators.required]],
    });
  }

  checkPwd(submitData) {
    if (this.resetPasswordForm.get('newPwd').value === this.resetPasswordForm.get('confirmpwd').value) {
      this.resetPasswordForm.get('confirmpwd').setErrors({ noMatch: true });
      this.password = true;
      this.submitRestPwdForm(submitData);
    } else {
      alert('Password Not Matching');
    }
  }

  submitRestPwdForm(submitData) {
    this.password = true;
    submitData.userName = this.userName;
    const url = RESET_PASSWORD;
    const headers = this.fs.getHttpHeaders();
    this.http.post(url, submitData, {
      withCredentials: true,
      headers: headers
    })
      .subscribe(resetData => {
        this.toastr.successToastr(this.globals.passwordUpdateMsg, 'Success!', {position: 'bottom-right'});
      },
        err => {
          this.toastr.warningToastr(err['statusText'], 'Alert!', {position: 'bottom-right'});
        });
  }

}

