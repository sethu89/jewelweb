import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileComponent } from './user-profile.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UserProfileRoutingModule } from './user-profile-routing.module';
import { FormsModule } from '@angular/forms';
import { Globals } from '../../shared/global';
import { UserProfileEditComponent } from './user-profile-edit/user-profile-edit.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule, FormsModule, UserProfileRoutingModule],
  declarations: [
    UserProfileComponent,
    UserProfileEditComponent,
    ResetPasswordComponent,

  ],

  entryComponents: [],
  bootstrap: [ResetPasswordComponent],
  providers: [Globals],

})
export class UserProfileModule { }
