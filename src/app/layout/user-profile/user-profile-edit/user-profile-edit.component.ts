import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { USER_DETAIl} from '../../../shared/services/endpoints';
import {HttpClient} from '@angular/common/http';
import {FactoryService} from '../../../shared/services';

@Component({
  selector: 'app-user-profile-edit',
  templateUrl: './user-profile-edit.component.html',
  styleUrls: ['./user-profile-edit.component.scss']
})
export class UserProfileEditComponent implements OnInit {
  @Output() itemFound = new EventEmitter<number>();
  userProfileForm: FormGroup;
  isAdmin = false;
  userName: String;

  constructor(private formBuilder: FormBuilder, private http: HttpClient,
              private fs: FactoryService) {
    this.userName = localStorage.getItem('loggedUser');
    this.isAdmin = this.fs.isUserAdmin();

  }

  ngOnInit() {
    this.createUserForm();
    this.retrieveUserFromService();
    // this.userProfileForm.disable();
  }

  createUserForm() {
    this.userProfileForm = this.formBuilder.group({
      firstName: '',
      lastName: '',
      userName: '',
    });
  }
  displayUserForm(userData) {
    this.userProfileForm = this.formBuilder.group({
      id: userData.id,
      firstName: userData.firstName,
      lastName: userData.lastName,
      userName: userData.userName,
    });
  }

  retrieveUserFromService() {
    // this.appComponent.loader(true);
    const url = USER_DETAIl + this.userName;
    const headers = this.fs.getHttpHeaders();
    this.http.get<any>(url, {
      withCredentials: true,
      headers: headers
    })
      .subscribe(returnUserdata => {
        if (returnUserdata) {
          this.displayUserForm(returnUserdata);
        }
        },
        err => {
          console.log(err);
        });
  }

  submitUserProfileForm(data) {
  }
  changePasswrd() {
    this.itemFound.emit(0);
  }
}
