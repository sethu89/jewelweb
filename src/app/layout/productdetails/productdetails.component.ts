import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ToastrManager} from 'ng6-toastr-notifications';
import {Globals} from '../../shared/global';
import {BarCodeService} from '../../shared/services/barcode.service';
import {ProductMasterService} from '../../shared/services/productmaster.service';
import {AppComponent} from '../../app.component';
import {BarcodedetailsService} from '../../shared/services/barcodedetails.service';
import {PicklistContainer} from '../../model/picklistContainer';
import {saveAs} from 'file-saver';
import {element} from 'protractor';
import {PicklistService} from '../../shared/services';

@Component({
    selector: 'app-productdetails',
    templateUrl: './productdetails.component.html',
    styleUrls: ['./productdetails.component.scss']
})
export class ProductdetailsComponent implements OnInit {

    productDetailForm: FormGroup;
    productMasterLists = [];
    productCodeArry = [];
    showProductDetailsRow = false;
    dropdownProductList = [];
    dropdownProductListArray = [];
    selectedItems = [];
    dropdownSettings = this.globals.dropdownSettings('Select BarCode', true);
    finalStatusValue = new Map<String, String>();
    currentFinalStatus = '';

    constructor(private fb: FormBuilder, public toastr: ToastrManager, public globals: Globals, public barCodeService: BarCodeService,
                private productMasterService: ProductMasterService, public appComponent: AppComponent, private pick: PicklistService) {
    }

    ngOnInit() {
        this.createProductDetailsForm();
        this.getProduct();
        this.fetchPickListValues();

    }

    createProductDetailsForm() {
        this.productDetailForm = this.fb.group({
            id: null,
            barCode: null,
            productStatus: null,
            processStatus: null,
            productMasterId: null,
            productId: [null],
            finalStatus: [null],
            customerName: [null],
            remarks: [null],
        });
    }

    getProduct() {
        this.productMasterLists = [];
        this.productMasterService.fetchAllProductMasterData().subscribe(data => {
            if (data.length > 0) {
                Object.keys(data).forEach((returnelem) => {
                    this.productMasterLists.push(new PicklistContainer(data[returnelem].id, data[returnelem].name));
                    this.productCodeArry[data[returnelem].id] = data[returnelem].barCodeChar;
                });
            }
        });
    }

    onChangeProduct(productMasterId: number) {
        this.appComponent.loader(true);
        this.productMasterService.fetchProductListByProductMasterById(productMasterId).subscribe(data => {
            console.log(data);
            if (data) {
                Object.keys(data).forEach((index) => {
                    this.dropdownProductList.push(this.globals.multiSelectItem(data[index].id, data[index].barCode));
                    // console.log(data.id, data.barCode);
                    this.dropdownProductListArray[data[index].id] = data[index];
                });
            } else {
                this.toastr.infoToastr('No Product finish product found', 'Info!', {position: 'bottom-right'});
            }
            this.appComponent.loader(false);

        });
    }

    OnItemSelectPart(item: any) {
        const productDetails = this.dropdownProductListArray[item.id];
        this.currentFinalStatus = productDetails['finalStatus'];
        this.productDetailForm.controls['finalStatus'].setValue(productDetails['finalStatus']);
        this.productDetailForm.controls['customerName'].setValue(productDetails['customerName']);
        this.productDetailForm.controls['remarks'].setValue(productDetails['remarks']);
        this.productDetailForm.controls['id'].setValue(item.id);
        this.productDetailForm.controls['barCode'].setValue(productDetails['barCode']);
        this.productDetailForm.controls['productStatus'].setValue(productDetails['productStatus']);
        this.productDetailForm.controls['processStatus'].setValue(productDetails['processStatus']);

        this.showProductDetailsRow = true;
    }


    saveBarCodeDetails(productFormData) {
        this.appComponent.loader(true);
        this.productMasterService.saveProduct(productFormData).subscribe(returnData => {
            console.log(returnData);
            if (returnData) {
                this.toastr.successToastr('Successfully Product final status updated', 'Success!', {position: 'bottom-right'});
                this.productDetailForm.reset();
                this.showProductDetailsRow = false;
                this.dropdownProductList = [];
                this.selectedItems = [];
            }
            this.appComponent.loader(false);
        });
    }

    fetchPickListValues() {
        this.pick.fetchPickListValues().subscribe(dataReturned => {
            const loc = <Map<String, Map<String, String>>>dataReturned;
            this.finalStatusValue = loc.get('FINAL_STATUS');

        });
    }

    onChangeFinalStatus(code) {
        if (this.currentFinalStatus === 'SLD') {
            if (code === 'BRK') {
                this.currentFinalStatus = 'BRK';
                alert('Check Before change status form Sold to Broken');
            } else if (code === 'STK') {
                this.currentFinalStatus = 'STK';
                alert('Check Before change status form Sold to Stock');
            }
        } else if (this.currentFinalStatus === 'BRK') {
            if (code === 'SLD') {
                this.currentFinalStatus = 'SLD';
                alert('Check Before change status form Broken to Sold');
            } else if (code === 'STK') {
                this.currentFinalStatus = 'STK';
                alert('Check Before change status form Broken to Stock');
            }
        } else if (this.currentFinalStatus === 'STK') {
            if (code === 'SLD') {
                this.currentFinalStatus = 'SLD';
            } else if (code === 'BRK') {
                this.currentFinalStatus = 'BRK';
            }
        }
    }

}
