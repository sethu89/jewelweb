import {NgModule} from '@angular/core';
import {Router, RouterModule, Routes} from '@angular/router';
import {ProductdetailsComponent} from './productdetails.component';

const routes: Routes = [
  {
    path: '',
    component: ProductdetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductdetailsRoutingModule {
  constructor(private router: Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }
}
