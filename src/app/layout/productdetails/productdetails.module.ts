import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Globals} from '../../shared/global';

import {BarCodeService} from '../../shared/services/barcode.service';
import {ProductMasterService} from '../../shared/services/productmaster.service';
import {BarcodedetailsService} from '../../shared/services/barcodedetails.service';
import {ProductdetailsComponent} from './productdetails.component';
import {ProductdetailsRoutingModule} from './productdetails-routing.module';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {PicklistService} from '../../shared/services';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        ProductdetailsRoutingModule,
        FormsModule,
        AngularMultiSelectModule,
    ],
  declarations: [
    ProductdetailsComponent,
  ],
  entryComponents: [],
  providers: [ Globals, BarCodeService, ProductMasterService, BarcodedetailsService, PicklistService],

})
export class ProductdetailsModule  { }
