import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {JobTransactionComponent} from './jobtransaction.component';


const routes: Routes = [
    {
        path: '',
        component: JobTransactionComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class JobTransactionRoutingModule {}
