import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { Globals } from '../../shared/global';

import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {JobTransactionComponent} from './jobtransaction.component';
import {JobTransactionSearchComponent} from './jobtransaction-search/jobtransaction-search.component';
import {JobTransactionEditComponent} from './jobtransaction-edit/jobtransaction-edit.component';
import {JobTransactionRoutingModule} from './jobtransaction-routing.module';
import {JobTransactionService} from '../../shared/services/jobtransaction.service';
import {ProductMasterService} from '../../shared/services/productmaster.service';
import {Ng2DatetimePickerModule} from 'ng2-datetime-picker';
import {DeskService} from '../../shared/services/desk.service';
import {UserService} from '../../shared/services/user.service';


@NgModule({
    declarations: [
        JobTransactionComponent,
        JobTransactionSearchComponent,
        JobTransactionEditComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        ReactiveFormsModule,
        JobTransactionRoutingModule, FormsModule, BsDatepickerModule.forRoot(), AngularMultiSelectModule, Ng2DatetimePickerModule,
    ],

    entryComponents: [JobTransactionEditComponent],
    bootstrap: [JobTransactionEditComponent],
    providers: [JobTransactionEditComponent, UserService, DeskService, JobTransactionService, Globals, ProductMasterService],
})
export class JobTransactionModule {
}
