import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobTransactionComponent } from './jobtransaction.component';

describe('JobTransactionComponent', () => {
  let component: JobTransactionComponent;
  let fixture: ComponentFixture<JobTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
