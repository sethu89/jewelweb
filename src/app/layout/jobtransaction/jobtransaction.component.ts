import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-jobtransaction',
  templateUrl: './jobtransaction.component.html',
  styleUrls: ['./jobtransaction.component.scss']
})
export class JobTransactionComponent implements OnInit {

  showSearch: boolean;
  jobTransactionId: number;
  title = 'Job Transaction';

  constructor() {
    this.showSearch = true;

  }

  ngOnInit() {
  }
  hideSearch(jobTransactionId: number) {
    this.jobTransactionId = jobTransactionId;
    this.showSearch = false;
  }

}
