import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Globals} from '../../../shared/global';
import {AppComponent} from '../../../app.component';
import {ToastrManager} from 'ng6-toastr-notifications';
import {JobTransactionService} from '../../../shared/services/jobtransaction.service';
import {ProductMasterService} from '../../../shared/services/productmaster.service';
import {JOB_TRANSACTION_DATA} from '../../../shared/services/endpoints';
import {HttpClient} from '@angular/common/http';
import {FactoryService} from '../../../shared/services';
import {PicklistContainer} from '../../../model/picklistContainer';

@Component({
  selector: 'app-jobtransaction-search',
  templateUrl: './jobtransaction-search.component.html',
  styleUrls: ['./jobtransaction-search.component.scss']
})
export class JobTransactionSearchComponent implements OnInit {

  @Output() itemFound = new EventEmitter<number>();
  jobTransactionSearchForm: FormGroup;

  message: string;
  showSearchResult = false;
  searchResult = [];
  title = 'JobTransactionSearch';
  value: any;
  formbuilder: any;

  dropProductMasterList = [];
  productMasterId = [];

  productDropdownSettings = {     // settings for multiselect dropdown
    singleSelection: true,
    text: 'Select Product',
    enableSearchFilter: true,
    classes: 'myclass custom-class',
  };

  constructor(private fb: FormBuilder,
              private http: HttpClient,
              private fs: FactoryService, private jobTransactionService: JobTransactionService, public appComponent: AppComponent,
              public globals: Globals, public toastr: ToastrManager, private productMasterService: ProductMasterService) {


    this.createSearchForm(this.fb);

  }
  ngOnInit() {
 this.retrieveAllProductMaster();
  }

  retrieveAllProductMaster() {
      this.productMasterService.fetchAllProductMasterData().subscribe(data => {
          if (data.length > 0) {
              Object.keys(data).forEach((element) => {
                  this.dropProductMasterList.push({'id': data[element].id, 'itemName': data[element].name});
              });
              this.dropProductMasterList.sort((a, b) => (a.itemName > b.itemName) ? 1 : -1);

          }
    }, err => {
      this.toastr.warningToastr(err['statusText'], 'Alert!', { position: 'bottom-right' });
      this.appComponent.loader(false);
    });
  }

  OnItemDeSelectProduct(item: any) {
    this.productMasterId = [];
    this.jobTransactionSearchForm.controls.productMasterId.setValue('');
  }
  createSearchForm(fb: FormBuilder) {
    this.jobTransactionSearchForm = fb.group({
        'reportFromDate': new Date(),
        'reportToDate': new Date(),
         productId: '',
    });
  }
  submitJobTransactionForm(postData) {
    let searchData = '';

    for (const field of Object.keys(postData)) {
      if (postData[field]) {
        searchData = searchData + field + '=' + encodeURIComponent(postData[field]) + '&';
      }
    }
    if(this.productMasterId.length > 0) {
        searchData += 'productMasterId=' + this.productMasterId[0]['id'];
    }
    this.getDataToService(searchData);
    this.appComponent.loader(false);
  }

  getDataToService(postData) {
    this.appComponent.loader(true);
      const url = JOB_TRANSACTION_DATA + postData;
      const headers = this.fs.getHttpHeaders();
      this.http.get<Array<any>>(url, {
          withCredentials: true,
          headers: headers
      }).subscribe(data => {
        if (data.length > 0) {
        this.showSearchResult = true;
        this.searchResult = data;
        this.toastr.successToastr(data.length + this.globals.jobTransactionSearchMsg, 'Success!', { position: 'bottom-right' });

      } else {
        this.showSearchResult = false;
        this.toastr.infoToastr(this.globals.jobTransactionNoDataMsg, 'Info!', { position: 'bottom-right' });

      }
      this.appComponent.loader(false);
    });
  }
  onViewJobTransaction(item) {
    this.itemFound.emit(item.jobTransactionId);
  }



  // onAddJobTransaction() {
  //   this.itemFound.emit(0);
  // }


}
