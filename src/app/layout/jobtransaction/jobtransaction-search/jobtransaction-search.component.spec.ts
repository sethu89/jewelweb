import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobTransactionSearchComponent } from './jobtransaction-search.component';

describe('JobTransactionSearchComponent', () => {
  let component: JobTransactionSearchComponent;
  let fixture: ComponentFixture<JobTransactionSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobTransactionSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobTransactionSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
