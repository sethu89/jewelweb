import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Globals} from '../../../shared/global';
import {AppComponent} from '../../../app.component';
import {ToastrManager} from 'ng6-toastr-notifications';
import {JobTransaction} from '../../../model/jobtransaction';
import {JobTransactionService} from '../../../shared/services/jobtransaction.service';
import {ProductMasterService} from '../../../shared/services/productmaster.service';
import {PicklistService} from '../../../shared/services';
import {DeskService} from '../../../shared/services/desk.service';
import {UserService} from '../../../shared/services/user.service';

@Component({
  selector: 'app-jobtransaction-edit',
  templateUrl: './jobtransaction-edit.component.html',
  styleUrls: ['./jobtransaction-edit.component.scss']
})
export class JobTransactionEditComponent implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('jobTransactionId') jobTransactionIdEdited: number;
  jobTransactionEditForm: FormGroup;
  statusValue = new Map<String, String>();
  productMasterMap = new Map<String, String>();
  deskMap = new Map<String, String>();
  userValueMap = new Map<Number, String>();


  inTime = new Date();
  outTime = new Date();

  dropProductList = [];
  selectedProductId = [];

  dropDeskList = [];
  selectedDeskId = [];

  dropUserList = [];
  selectedUserId = [];

  dropdownUser = [];


  productDropdownSettings = {     // settings for multiselect dropdown
    singleSelection: true,
    text: 'Select Product',
    enableSearchFilter: true,
    classes: 'myclass custom-class',
  };

  deskDropdownSettings = {     // settings for multiselect dropdown
    singleSelection: true,
    text: 'Select Desk',
    enableSearchFilter: true,
    classes: 'myclass custom-class',
  };

  userDropdownSettings = {     // settings for multiselect dropdown
    singleSelection: true,
    text: 'Select User',
    enableSearchFilter: true,
    classes: 'myclass custom-class',
  };
  title: string;
  constructor(private fb: FormBuilder, private jobTransactionService: JobTransactionService, public appComponent: AppComponent,
              public globals: Globals, public toastr: ToastrManager, private productMasterService: ProductMasterService,
              private pick: PicklistService, private deskService: DeskService, public usrService: UserService) {
    this.createEditForm(this.fb);
  }

  ngOnInit() {
    this.fetchPickListValues();
    this.retrieveAllProduct();
    this.retrieveAllDesk();
    this.fetchPickListUser();
    // this.createNewJobTransaction();

  }
  // createNewJobTransaction() {
  //   this.title = this.globals.createTitle;
  //   this.createEditForm(this.fb);
  // }
  createEditForm(fb: FormBuilder) {
    this.jobTransactionEditForm = fb.group({
      productId: '',
      userId: '',
      deskId: '',
      inTime: new Date(),
      outTime: new Date(),
      jobType: '',
      status: ''

    });
  }
  displayRetrievedJobTransactionForm(fb: FormBuilder, retrieved) {
    this.jobTransactionEditForm = fb.group({
      id: retrieved.id,
      productId: retrieved.productId,
      userId: retrieved.userId,
      deskId: retrieved.deskId,
      inTime: new Date(retrieved.inTime),
      outTime: new Date(retrieved.outTime),
      jobType: retrieved.jobType,
      status: retrieved.status,
    });
    this.inTime = new Date(retrieved.inTime);
    this.outTime = new Date(retrieved.outTime);
    this.selectedProductId.push(this.globals.multiSelectItem(retrieved.productId, this.productMasterMap.get(retrieved.productId)));
    this.selectedDeskId.push(this.globals.multiSelectItem(retrieved.deskId, this.deskMap.get(retrieved.deskId)));
    this.selectedUserId.push(this.globals.multiSelectItem(retrieved.userId, this.userValueMap.get(retrieved.userId)));

  }
  retrieve() {
    this.title = this.globals.editTitle;
    this.retrieveJobTransaction();
  }

  retrieveJobTransaction() {
    this.appComponent.loader(true);
    this.jobTransactionService.fetchJobTransactionById(this.jobTransactionIdEdited).subscribe(data => {
          this.displayRetrievedJobTransactionForm(this.fb, data);
        },
        err => {
          this.toastr.warningToastr(err['statusText'], 'Alert!', { position: 'bottom-right' });
        });
    this.appComponent.loader(false);
  }

  submitJobTransactionForm(postData) {
    this.appComponent.loader(true);
    if (this.jobTransactionIdEdited > 0) {
      postData.id = this.jobTransactionIdEdited;
    }
    if (this.selectedProductId.length > 0) {
      postData.productId = this.selectedProductId[0].id;
    }
    if (this.selectedDeskId.length > 0) {
      postData.deskId = this.selectedDeskId[0].id;
    }
    if (this.selectedUserId.length > 0) {
      postData.userId = this.selectedUserId[0].id;
    }
    this.jobTransactionService.saveJobTransaction(postData).subscribe(data => {
          if (this.jobTransactionIdEdited > 0) {
            this.toastr.successToastr(this.globals.jobTransactionUpdatedMsg, 'Success!', { position: 'bottom-right' });

          } else {
            this.toastr.successToastr(this.globals.jobTransactionAddedMsg, 'Success!', { position: 'bottom-right' });
          }
          this.jobTransactionIdEdited = data['id'];
        },
        err => {
          this.toastr.warningToastr(err['statusText'], 'Alert!');
        });
    this.appComponent.loader(false);
  }

  retrieveAllProduct() {
    this.productMasterService.fetchAllProductList().subscribe(data => {
      this.dropProductList = data['productMasterList'];
      this.productMasterMap = data['productMasterMap'];
    }, err => {
      this.toastr.warningToastr(err['statusText'], 'Alert!', { position: 'bottom-right' });
      this.appComponent.loader(false);
    });
  }

  retrieveAllDesk() {
    this.deskService.fetchAllDeskList().subscribe(data => {
      this.dropDeskList = data['deskList'];
      this.deskMap = data['deskMap'];

    }, err => {
      this.toastr.warningToastr(err['statusText'], 'Alert!', { position: 'bottom-right' });
      this.appComponent.loader(false);
    });
  }


  OnItemDeSelectProduct(item: any) {
    this.selectedProductId = [];
    this.jobTransactionEditForm.controls.productMasterId.setValue('');
  }

  OnItemDeSelectDesk(item: any) {
    this.selectedDeskId = [];
    this.jobTransactionEditForm.controls.deskId.setValue('');
  }


  OnItemDeSelectUser(item: any) {
    this.selectedUserId = [];
    this.jobTransactionEditForm.controls.userId.setValue('');
  }

  fetchPickListValues() {
    this.statusValue.set('Approved', 'Approved');
    this.statusValue.set('Rejected', 'Rejected');
  }

  fetchPickListUser() {
    this.usrService.searchAllUser().subscribe(userAllData => {
          Object.keys(userAllData).forEach((key) => {
            this.dropUserList.push(this.globals.multiSelectItem(key, userAllData[key]));
            this.userValueMap.set(Number(key), userAllData[key]);
          });
          if (this.jobTransactionIdEdited > 0) {
            this.retrieve();
          }
        },
        err => {
          this.toastr.warningToastr(this.globals.error, 'Alert!');
          this.appComponent.loader(false);
        });
  }


}
