import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobTransactionEditComponent } from './jobtransaction-edit.component';

describe('JobTransactionEditComponent', () => {
  let component: JobTransactionEditComponent;
  let fixture: ComponentFixture<JobTransactionEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobTransactionEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobTransactionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
