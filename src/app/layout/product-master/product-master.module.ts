import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Globals } from '../../shared/global';
import {ProductMasterEditComponent} from './product-master-edit/product-master-edit.component';
import {ProductMasterSearchComponent} from './product-master-search/product-master-search.component';
import {ProductMasterComponent} from './product-master.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ProductMasterRoutingModule} from './product-master-routing.module';
import { PropAttributesComponent } from './prop-attributes/prop-attributes.component';
import {NgxLoadingModule} from 'ngx-loading';
import {MatDatepickerModule, MatFormFieldModule, MatNativeDateModule} from '@angular/material';
import {ProductMasterService} from '../../shared/services/productmaster.service';
import {UploadService} from '../../shared/services/upload.service';
import {PicklistService} from '../../shared/services';




@NgModule({
  declarations: [
    ProductMasterComponent,
    ProductMasterSearchComponent,
    ProductMasterEditComponent,
    PropAttributesComponent,
  ],
  imports: [
    CommonModule, FormsModule,
    ReactiveFormsModule, ProductMasterRoutingModule,
    ReactiveFormsModule,
    NgxLoadingModule.forRoot({}),
    MatFormFieldModule,
    MatNativeDateModule,
    MatDatepickerModule,
  ],

  entryComponents: [ProductMasterEditComponent, PropAttributesComponent],
  bootstrap: [],
  providers: [ProductMasterEditComponent, UploadService, PicklistService, ProductMasterService, Globals],
})


export class ProductMasterModule {

}
