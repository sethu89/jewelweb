import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-master',
  templateUrl: './product-master.component.html',
  styleUrls: ['./product-master.component.scss']
})
export class ProductMasterComponent implements OnInit {

  showSearch: boolean;
  productMasterId: number;
  title = 'Product Master';

  constructor() {
    this.showSearch = true;

  }

  ngOnInit() {
  }
  hideSearch(productMasterId: number) {
    this.productMasterId = productMasterId;
    this.showSearch = false;
  }

}
