import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropAttributesComponent } from './prop-attributes.component';

describe('PropAttributesComponent', () => {
  let component: PropAttributesComponent;
  let fixture: ComponentFixture<PropAttributesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropAttributesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropAttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
