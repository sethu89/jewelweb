import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FactoryService, PicklistService} from '../../../shared/services';
import {DialogData} from '../prop-attributes-interface';


@Component({
  selector: 'app-prop-attributes',
  templateUrl: './prop-attributes.component.html',
  styleUrls: ['./prop-attributes.component.scss']
})
export class PropAttributesComponent implements OnInit {

  propAttributesDialog: FormGroup;

  unitValue = new Map<String, String>();
  productLabelValue = new Map<String, String>();


  edit: boolean;


  constructor(public dialogRef: MatDialogRef<PropAttributesComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData,
              private fb: FormBuilder, private fs: FactoryService, private pick: PicklistService) {
    if (this.data.value != null) {
      this.edit = true;
      this.fillPropAttributesForm(this.data);
    } else {
      this.createPropAttributesForm();
      this.edit = false;
    }
    this.fetchPickListValues();
  }

  ngOnInit() {
  }

  createPropAttributesForm() {
    this.propAttributesDialog = this.fb.group({
      id: [null],
      label: [null],
      value: [null],
      unit: [null],
      remarks: [null],
    });
  }


  fillPropAttributesForm(value) {
    this.propAttributesDialog = this.fb.group({
      id: value.id,
      label: value.label,
      value: value.value,
      unit: value.unit,
      remarks: value.remarks,
    });
  }

  submitPropAttributesForm(postData) {
      this.dialogRef.close(postData);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  fetchPickListValues() {
    this.pick.fetchPickListValues().subscribe(data => {
      const loc = <Map<String, Map<String, String>>>data;
      this.unitValue = loc.get('PRODUCT_WT_UNIT');
      this.productLabelValue = loc.get('PRODUCT_LABEL');
    });
  }

}
