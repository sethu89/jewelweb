import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ToastrManager} from 'ng6-toastr-notifications';
import {AppComponent} from '../../../app.component';
import {MatDialog} from '@angular/material';
import {PropAttributesComponent} from '../prop-attributes/prop-attributes.component';
import {ProductMasterService} from '../../../shared/services/productmaster.service';
import {Globals} from '../../../shared/global';
import {ProductMasterForm} from '../../../model/productmaster';
import {FactoryService, PicklistService} from '../../../shared/services';
import {UploadService} from '../../../shared/services/upload.service';
import {HttpClient} from '@angular/common/http';
import {DOWNLOAD_FILE} from '../../../shared/services/endpoints';
import {saveAs} from 'file-saver';


@Component({
    selector: 'app-product-master-edit',
    templateUrl: './product-master-edit.component.html',
    styleUrls: ['./product-master-edit.component.scss']
})
export class ProductMasterEditComponent implements OnInit {
// tslint:disable-next-line:no-input-rename
    @Input('productMasterId') productMasterIdEdited: number;
    ProductMasterEditForm: FormGroup;
    title: string;
    statusValue = new Map<String, String>();

    index: number;
    propAttributesEdit: boolean;
    propAttributesList = new Array<any>();

    selectedFileName;
    selectedFile;
    productFileName;

    isImageChosen = false;

    imgForm: FormGroup;

    documentType = 'productImage';    // uploading image
    error: string;
    uploadResponse = {status: '', message: '', filePath: ''};
    imgSrc = [];


    propData: ProductMasterForm;

    showPropAttributes = false;

    constructor(private fb: FormBuilder, public appComponent: AppComponent, public dialog: MatDialog,
                public toastr: ToastrManager, private productMasterService: ProductMasterService,
                private globals: Globals, private pick: PicklistService, private uploadService: UploadService,
                private http: HttpClient, private fs: FactoryService) {
        this.createEditForm(this.fb);
    }

    ngOnInit() {
        this.createNewProductMaster();
        this.fetchPickListValues();
        if (this.productMasterIdEdited > 0) {
            this.retrieve();
        }
    }

    onFileChange(event) {                  // when img file is chosen
        if (event.target.files.length > 0) {
            this.selectedFileName = event.target.files[0].name;
            this.selectedFile = event.target.files[0];
            const file = event.target.files[0];
            this.imgForm.get('file').setValue(file);
            // for displaying image preview
            const reader = new FileReader();
            reader.onload = e => this.imgSrc[0] = reader.result;
            reader.readAsDataURL(file);
            this.isImageChosen = true;
        }
    }

    createNewProductMaster() {
        this.title = this.globals.createTitle;
        this.createEditForm(this.fb);
    }

    createEditForm(fb: FormBuilder) {
        this.ProductMasterEditForm = fb.group({
            name: [null],
            status: 'true',
            propAttributes: [null],
            barCodeChar: [null],
            imagePath: [null]
        });
        this.imgForm = fb.group({
            file: '',
            img: '',
            packEndTime: '',
        });
    }

    displayRetrievedProductMasterForm(fb: FormBuilder, retrieved: ProductMasterForm) {
        this.ProductMasterEditForm = fb.group({
            id: retrieved.id,
            name: retrieved.name,
            barCodeChar: retrieved.barCodeChar,
            imagePath: retrieved.imagePath,
            status: retrieved.status
        });
        this.productFileName = retrieved.imagePath;
        this.imgForm = fb.group({
            file: retrieved.file,
            img: retrieved.img,
        });
    }

    retrieve() {
        this.title = this.globals.editTitle;
        this.retrieveProductFromService();
        this.fetchPropAttributesByProductMasterId();
    }

    retrieveProductFromService() {
        this.productMasterService.fetchProductMasterById(this.productMasterIdEdited).subscribe(returnData => {
                this.propData = returnData;
                this.displayRetrievedProductMasterForm(this.fb, returnData);
            },
            err => {
                this.toastr.warningToastr(err['statusText'], 'Alert!', {position: 'bottom-right'});
                this.appComponent.loader(false);
            });
        this.appComponent.loader(false);
    }

    openEditPropAttributesDialog(data, index) {
        this.index = index;
        const dialogRef = this.dialog.open(PropAttributesComponent, {
            width: '1000px',
            data: {
                id:  data.id,
                label: data.label,
                value: data.value,
                unit: data.unit,
                index: index,
                remarks: data.remarks
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result != null) {
                if (this.index >= 0) {
                    this.propAttributesEdit = true;
                }
                this.submitPropAttributesList(result);
            }
        });
    }

    openAddPropAttributesDialog() {
        const dialogRef = this.dialog.open(PropAttributesComponent, {
            width: '1000px',
            data: {
                label: null,
                value: null,
                unit: null,
                remarks: null
                // pkgNo: this.pkgNo,
                // index: null,
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result != null) {
                this.submitPropAttributesList(result);
                // this.pkgNo += 1;
            }
        });
    }

    submitPropAttributesList(propAttributeData) {

        if (this.productMasterIdEdited > 0) {
            propAttributeData.poId = this.propData.id;
        }

        if (this.propAttributesEdit) {
            this.propAttributesList[this.index] = propAttributeData;
        } else {
            this.propAttributesList.push(propAttributeData);
        }
        this.propAttributesEdit = false;
        this.index = null;
        this.showPropAttributes = true;
    }

    fetchPropAttributesByProductMasterId() {
        this.productMasterService.fetchPropAttributesByProductMasterId(this.productMasterIdEdited).subscribe(data => {
            this.propAttributesList = data;
        });
    }

    // upload image in folder
    saveImgFolder(imageName) {
        const formData = new FormData();
        formData.append('documentType', this.documentType);
        formData.append('file', this.imgForm.get('file').value, imageName);
        this.uploadService.upload(formData).subscribe(
            (res) => this.uploadResponse = res,
            (err) => this.error = err
        );
    }

    duplicateCheck(postData) {
        this.appComponent.loader(true);
        const searchData = 'name=' + encodeURIComponent(postData.name) + '&exactMatch=true';
        this.productMasterService.fetchAllProductMaster(searchData).subscribe(data => {
            if (data && data.length > 0) {
                if (postData.id && postData.id === data[0].id) {
                    const barCodeData = 'barCodeChar=' + encodeURIComponent(postData.barCodeChar) + '&exactMatch=true';
                    this.productMasterService.fetchAllProductMaster(barCodeData).subscribe(barCodeDataResult => {
                        if (barCodeDataResult && barCodeDataResult.length > 0) {
                            if (postData.id && postData.id === barCodeDataResult[0].id) {
                                this.submitProductEditForm(postData);
                            } else {
                                this.toastr.infoToastr(postData.barCodeChar + this.globals.productBarcodeAlreadyExistsMsg,
                                    'Info!', {position: 'bottom-right'});
                                this.appComponent.loader(false);
                            }
                        } else {
                            this.submitProductEditForm(postData);
                        }
                    });
                } else {
                    this.toastr.infoToastr(postData.name + this.globals.productMasterAlreadyExistsMsg, 'Info!', {position: 'bottom-right'});
                    this.appComponent.loader(false);
                }
            } else {
                this.submitProductEditForm(postData);
            }
        });
    }

    submitProductEditForm(postData) {
        this.appComponent.loader(true);
        if (this.productMasterIdEdited > 0) {
            postData.id = this.productMasterIdEdited;
        }
        let type;
        if (this.isImageChosen === true) {
            type = this.imgForm.get('file').value.name.substr(this.imgForm.get('file').value.name.lastIndexOf('.'));
            postData.imagePath = 'Product_' + postData.name + type;
        }
        postData.propAttributes = this.propAttributesList;
        this.productMasterService.saveProductMaster(postData).subscribe(data => {
                if (this.isImageChosen === true) {
                    this.saveImgFolder(postData.imagePath);
                }
                if (this.productMasterIdEdited > 0) {
                    this.toastr.successToastr(this.globals.productMasterUpdatedMsg, 'Success!', {position: 'bottom-right'});

                } else {
                    this.toastr.successToastr(this.globals.productMasterAddedMsg, 'Success!', {position: 'bottom-right'});
                }
                this.productMasterIdEdited = data['id'];
                this.selectedFileName = '';
            },
            err => {
                this.toastr.warningToastr(err['statusText'], 'Alert!', {position: 'bottom-right'});
            });
        this.appComponent.loader(false);
    }

    fetchPickListValues() {
        this.pick.fetchPickListValues().subscribe(dataReturned => {
            const loc = <Map<String, Map<String, String>>>dataReturned;
            this.statusValue = loc.get('ACTIVE_STATUS');

        });
    }


    getFile(fileName) {
        const url = DOWNLOAD_FILE + '?fileName=' + fileName + '&documentType=' + this.documentType;
        const headers = this.fs.getPdfHttpHeaders();
        this.http.get(url, {responseType: 'blob', headers: headers, withCredentials: true})
            .subscribe(returnData => {
                    const blob = new Blob([returnData], {type: 'application/pdf'});
                    saveAs(blob, `${fileName}`);
                },
                error1 => {
                    if (error1['status'] === 403) {
                        this.toastr.warningToastr('Unauthorized Access to Download', 'Alert!', {position: 'bottom-right'});
                    }
                }
            );
    }


}
