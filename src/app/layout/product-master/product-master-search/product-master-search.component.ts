import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AppComponent} from '../../../app.component';
import {Globals} from '../../../shared/global';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ProductMasterService} from '../../../shared/services/productmaster.service';

@Component({
  selector: 'app-product-master-search',
  templateUrl: './product-master-search.component.html',
  styleUrls: ['./product-master-search.component.scss']
})
export class ProductMasterSearchComponent implements OnInit {

  @Output() itemFound = new EventEmitter<number>();
  ProductMasterSearchForm: FormGroup;

  message: string;
  showSearchResult = false;
  searchResult = [];
  textcontent = [];
  value: any;
  formbuilder: any;

  constructor(private fb: FormBuilder, public appComponent: AppComponent, private productMasterService: ProductMasterService,
              public globals: Globals, public toastr: ToastrManager) {


    this.createSearchForm(this.fb);

  }
  ngOnInit() {

  }
  createSearchForm(fb: FormBuilder) {
    this.ProductMasterSearchForm = fb.group({
      name: [null],
    });
  }
  submitProductMasterForm(postData) {
    this.appComponent.loader(true);
    let searchData = '';

    for (const field of Object.keys(postData)) {
      if (postData[field]) {
        searchData = searchData + field + '=' + encodeURIComponent(postData[field]) + '&';
      }
    }
    this.getDataToService(searchData);
    this.appComponent.loader(false);
  }

  getDataToService(postData) {
    this.appComponent.loader(true);
    this.productMasterService.fetchAllProductMaster(postData).subscribe(data => {
      if (data.length > 0) {
        this.showSearchResult = true;
        this.searchResult = data;
        this.toastr.successToastr(data.length + this.globals.productMasterSearchMsg, 'Success!', {position: 'bottom-right'});

      } else {
        this.showSearchResult = false;
        this.toastr.infoToastr(this.globals.productMasterNoDataMsg, 'Info!', {position: 'bottom-right'});

      }
      this.appComponent.loader(false);
    });
  }
  onViewProductMaster(productMasterId) {
    this.itemFound.emit(productMasterId);
  }



  onAddProductMaster() {
    this.itemFound.emit(0);
  }


}
