import {NgModule} from '@angular/core';
import {Router, RouterModule, Routes} from '@angular/router';
import {BarcodeComponent} from './barcode.component';

const routes: Routes = [
  {
    path: '',
    component: BarcodeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BarCodeRoutingModule {
  constructor(private router: Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }
}
