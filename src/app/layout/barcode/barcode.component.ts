import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrManager} from 'ng6-toastr-notifications';
import {Globals} from '../../shared/global';
import {BarCodeService} from '../../shared/services/barcode.service';
import {ProductMasterService} from '../../shared/services/productmaster.service';
import {PicklistContainer} from '../../model/picklistContainer';
import {saveAs} from 'file-saver';
import {AppComponent} from '../../app.component';
import {BarcodedetailsService} from '../../shared/services/barcodedetails.service';

@Component({
  selector: 'app-barcode',
  templateUrl: './barcode.component.html',
  styleUrls: ['./barcode.component.scss']
})
export class BarcodeComponent implements OnInit {

  barcodeGenerateForm: FormGroup;
  productMasterLists = [];
  productCodeArry = [];
  barCodeChar = false;
  fromNumber;
  productBarcodeDetails = [];
  showProductDetailsResult = false;

  constructor(private fb: FormBuilder,  public toastr: ToastrManager, public globals: Globals, public barCodeService: BarCodeService,
              private productMasterService: ProductMasterService, public appComponent: AppComponent,
              private barcodedetailsService: BarcodedetailsService) {
  }

  ngOnInit() {
    this.createBarcodeGenerateForm();
    this.getProduct();
  }

  createBarcodeGenerateForm() {
    this.barcodeGenerateForm = this.fb.group({
      productMasterId: null,
      seriesNo: [null],
      fromNumber: null,
      noOfLables: [null],
      barCodeChar: null,
    });
  }

  getProduct() {
    this.productMasterLists = [];
    this.productMasterService.fetchAllProductMasterData().subscribe(data => {
      if (data.length > 0) {
        Object.keys(data).forEach((element) => {
          this.productMasterLists.push(new PicklistContainer(data[element].id, data[element].name));
          this.productCodeArry[data[element].id] = data[element].barCodeChar;
        });
      }
    });
  }
  //
  // onChangeProduct(productMasterId: number) {
  //   this.appComponent.loader(true);
  //   this.barCodeChar = true;
  //   this.barcodeGenerateForm.controls.seriesNo.setValue(this.productCodeArry[productMasterId]);
  //   this.productMasterService.fetchProductByProductMasterById(productMasterId).subscribe(data => {
  //     if (data) {
  //       this.barcodeGenerateForm.controls.fromNumber.setValue(data);
  //     }
  //     this.appComponent.loader(false);
  //   });
  // }
  onChangeProduct(productMasterId: number) {
    this.appComponent.loader(true);
    this.barCodeChar = true;
    this.barcodeGenerateForm.controls.seriesNo.setValue(this.productCodeArry[productMasterId]);
    this.barcodedetailsService.fetchLastGeneratedBarcodeByProductMasterId(productMasterId).subscribe(data => {
      if (data) {
        this.barcodeGenerateForm.controls.fromNumber.setValue(data);
        this.fetchBarcodeDetailList(productMasterId);
      }
      this.appComponent.loader(false);
    });
  }

  fetchBarcodeDetailList(productMasterId: number) {
    this.barcodedetailsService.fetchBarCodeDetailsListByProductMasterId(productMasterId).subscribe(data => {
      if (data) {
        this.productBarcodeDetails = data;
        this.showProductDetailsResult = true;
      } else {
        this.productBarcodeDetails = [];
        this.showProductDetailsResult = false;
      }
      this.appComponent.loader(false);
    });
  }

  saveBarCodeDetails(data) {
    this.barcodedetailsService.saveBarcodeDetails(data).subscribe(returnData => {
      if (returnData) {
        this.fetchBarcodeDetailList(data.productMasterId);
      }
      this.appComponent.loader(false);
    });
  }


  submitBarcodeGenerateForm(postBarcodeFormData) {
    this.appComponent.loader(true);
    this.generateBarcode(postBarcodeFormData.seriesNo, postBarcodeFormData.fromNumber,
        postBarcodeFormData.noOfLables, postBarcodeFormData);
    this.barcodeGenerateForm.reset();
    this.showProductDetailsResult = false;
    this.productBarcodeDetails = [];
  }

  generateBarcode(seriesNo, fromNumber, noOfLables, postBarcodeFormData ) {
    this.barCodeService.generateBarCode(seriesNo, fromNumber,
        noOfLables).subscribe( returnData => {
          if (postBarcodeFormData) {
            this.saveBarCodeDetails(postBarcodeFormData);
          }
      const blob = new Blob([returnData], {type: 'application/pdf'});
      saveAs(blob, `${seriesNo  + ' ' + fromNumber + '-' + (fromNumber + (noOfLables - 1))}.pdf`);
      this.toastr.successToastr('Barcode Generate Successfully for '
          + seriesNo  + ' ' + fromNumber + '-' + (fromNumber + (noOfLables - 1)), 'Success!', { position: 'bottom-right' });
      this.appComponent.loader(false);
    });
  }

  reGenrateBarCode(startCode, fromNumber, noOflabels) {
    let str = startCode;
    str = str.substring(0, str.length - 6);
    this.appComponent.loader(true);
    console.log(str);
    this.generateBarcode(str, fromNumber, noOflabels, null);
  }

}
