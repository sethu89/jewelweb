import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Globals} from '../../shared/global';
import {BarcodeComponent} from './barcode.component';
import {BarCodeRoutingModule} from './barcode-routing.module';
import {BarCodeService} from '../../shared/services/barcode.service';
import {ProductMasterService} from '../../shared/services/productmaster.service';
import {BarcodedetailsService} from '../../shared/services/barcodedetails.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BarCodeRoutingModule,
    FormsModule,
  ],
  declarations: [
    BarcodeComponent,
  ],
  entryComponents: [],
  providers: [ Globals, BarCodeService, ProductMasterService, BarcodedetailsService],

})
export class BarCodeModule  { }
