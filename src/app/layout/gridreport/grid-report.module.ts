import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {GridReportRoutingModule} from './grid-report-routing.module';

import {GridReportComponent} from './grid-report.component';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {Globals} from '../../shared/global';
import {ProductMasterService} from '../../../app/shared/services/productmaster.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { GridSearchComponent } from './grid-search/grid-search.component';
import {TranslateModule} from '@ngx-translate/core';
import {DataTablesModule} from 'angular-datatables';

@NgModule({
  imports: [
    CommonModule, ReactiveFormsModule, GridReportRoutingModule, BsDatepickerModule.forRoot(),
    AngularMultiSelectModule, FormsModule, TranslateModule, DataTablesModule
  ],

  declarations: [
    GridReportComponent,
    GridSearchComponent
  ],
  entryComponents: [],
  providers: [ Globals, ProductMasterService],

})
export class GridReportModule  { }
