import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FactoryService, PicklistService} from '../../shared/services';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs';
import {GridReportService} from '../../shared/services/gridreport.service';


@Component({
  selector: 'app-report',
  templateUrl: './grid-report.component.html',
  styleUrls: ['./grid-report.component.scss']
})

export class GridReportComponent implements AfterViewInit, OnDestroy, OnInit {
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;

  title = 'Report';
  showGrid = false;
  reportType = 'productMaster';
  reportTitle = '';
  footerDisplay = false;
  rowOnClick = false;
  // grid
  dataset: any[] = [];

  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  columns: any[] = [];
  columnSize: number;
  fixedCols = 0;
  multiHeader = false;

  constructor(private route: ActivatedRoute, private gridUtils: GridReportService,
              private router: Router, private fs: FactoryService, private pick: PicklistService) {
    this.fetchPickListValues();

    if (this.route.paramMap['destination'].value.reportType === 'productMaster') {
      this.reportType = 'productMaster';
      this.reportTitle = 'Product Master';
      this.fixedCols = 0;
      this.footerDisplay = true;
    } else if (this.route.paramMap['destination'].value.reportType === 'productDetail') {
      this.reportType = 'productDetail';
      this.reportTitle = 'Product Details';
      this.fixedCols = 0;
      this.footerDisplay = false;
      this.rowOnClick = true;
    } else if (this.route.paramMap['destination'].value.reportType === 'inventoryTransactions') {
      this.reportType = 'inventoryTransactions';
      this.reportTitle = 'Inventory Transactions';
      this.fixedCols = 0;
      this.footerDisplay = false;
    } else if (this.route.paramMap['destination'].value.reportType === 'deskReport') {
      this.reportType = 'deskReport';
      this.reportTitle = 'Desk Report';
      this.fixedCols = 0;
      this.footerDisplay = true;
    }
    this.columns = this.gridUtils.getGridColumns(this.reportType);
    this.columnSize = this.columns.length;
  }

   // sub: any
  showResults(gridData) {
    this.showGrid = true;
    this.setGridOptions(gridData.data, gridData.summaryText, this.fixedCols, this.footerDisplay, this.rowOnClick);
  }

  ngOnInit() {
  }

  setGridOptions(data, summaryText, fixedCols, footerDisplay, rowOnClick) {
    this.dataset = [];
    this.dataset = data;
    this.dtOptions = {
      data: data,
      columns: this.columns,
      scrollX: true,
      scrollY: true,
      searching: true,
      paging: true,
      pageLength: 10,
      dom: 'Bfrtip',
      rowGroup: { dataSrc: [0, 1]},
      initComplete:  function (settings, json) {
        $('.dt-button').removeClass('dt-button');
      },
      buttons: [
        { className: 'btn btn-success', extend: 'excelHtml5',
          footer: true, messageTop: this.reportTitle + ' Report - \n Search Criteria: ' + summaryText,
          autoFilter: true
        },
        { className: 'btn btn-success', extend: 'print',
          footer: true, messageTop: this.reportTitle + ' Report - \n Search Criteria: ' + summaryText},
        { className: 'btn btn-success', extend: 'pdfHtml5',
          footer: true, messageTop: this.reportTitle + ' Report - \n Search Criteria: ' + summaryText }
      ],
      fixedColumns: {
        leftColumns: fixedCols
      },
      'fnFooterCallback': function( tfoot, tabledata, start, end, display ) {
        if (footerDisplay) {
          const api = this.api();
          let cnt = 0;
          // JQuery statements start
          const colCnt = $(api.columns()[0]).length;
          $( api.column( 1 ).footer() ).html('Grand Total:');
          while (cnt < colCnt) {
            if ( $(api.column(cnt)) && $(api.column(cnt).footer()).hasClass('total')) {
              $(api.column(cnt).footer()).html(
                api.column(cnt).data().reduce(function (a, b) {
                  return (Math.floor( (a + b) * 100 ) / 100 );
                }, 0)
              );
            }
            cnt++;
          }
        }
      },

      'rowCallback': function( row, gridData, index ) {
        if ( gridData.status === 'Rejected' ) {
          $('td', row).css('background-color', 'Green');
        }
        if (rowOnClick) {
          $(row).on('click' , function () {
              $('#jobTransactionDiv').show();
            $('#jobTransactionTBody').empty();

            for (const jobTransaction in gridData['jobTransactionList']) {
              console.log(jobTransaction);
              const transDate = new Date (gridData['jobTransactionList'][jobTransaction]['inTime']);
              const strDate = transDate.getFullYear() + '-' + (transDate.getMonth() + 1) + '-' + transDate.getDate()
                  + ' ' + transDate.getHours()  + ':' + transDate.getMinutes() + ':' + transDate.getSeconds();
              let tr = '<tr><td>' + strDate + '</td>';
              tr += '<td>' + gridData['jobTransactionList'][jobTransaction]['deskName'] + '</td>';
              tr += '<td>' + gridData['jobTransactionList'][jobTransaction]['jobType'] + '</td>';
              tr += '<td>' + gridData['jobTransactionList'][jobTransaction]['status'] + '</td>';
              tr += '<td>' + gridData['jobTransactionList'][jobTransaction]['remarks'] + '</td>';

              $('#jobTransactionTBody').append(tr);
            }
          });
        }
    },
      // JQuery statement ends
    };
    setTimeout(() => {
      this.rerender();
    });
  }

  fetchPickListValues() {
    this.pick.fetchPickListValues().subscribe(dataReturned => {
      const loc = <Map<String, Map<String, String>>>dataReturned;
    });
  }

  ngAfterViewInit(): void {
    // this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    if (this.dtElement && this.dtElement.dtInstance) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    }
    this.dtTrigger.next();
  }
}
