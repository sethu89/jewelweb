import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {
  DESK_REPORT,
  INVENTORY_TRANSACTION_REPORT, PRODUCT_DETAIL_REPORT, PRODUCT_MASTER_REPORT
} from '../../../shared/services/endpoints';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {FactoryService, PicklistService, CommonutilsService} from '../../../shared/services';
import {DatePipe} from '@angular/common';
import { AppComponent } from '../../../app.component';
import {GridReportComponent} from '../grid-report.component';
import {ProductMasterService} from '../../../shared/services/productmaster.service';
import {Globals} from '../../../shared/global';
import {ToastrManager} from 'ng6-toastr-notifications';


@Component({
  selector: 'app-grid-search',
  templateUrl: './grid-search.component.html',
  styleUrls: ['./grid-search.component.scss']
})

export class GridSearchComponent implements OnInit {
  @Output() itemFound = new EventEmitter<any>();

  searchForm: FormGroup;
  reportType: string;
  searchParamMap = {'reportFromDate': 'Date From', 'reportToDate': 'Date To', 'productMasterId': 'Product Name'};

  reportDateFromTitle = '';
  reportDateToTitle = '';
  dropProductMasterList = [];
  productMasterId = [];

  productDropdownSettings = {     // settings for multiselect dropdown
    singleSelection: true,
    text: 'Select Product',
    enableSearchFilter: true,
    classes: 'myclass custom-class',
  };

  constructor(private fb: FormBuilder, private http: HttpClient, public globals: Globals,
              route: ActivatedRoute, private router: Router, private fs: FactoryService, private pick: PicklistService,
              public datepipe: DatePipe, public  reportComponent: GridReportComponent, public productMasterService: ProductMasterService,
              public appComponent: AppComponent, private utils: CommonutilsService, public toastr: ToastrManager) {
    this.createSearchCriteriaForm(this.fb);
  }

  ngOnInit() {
    this.retrieveAllProductMaster();
    this.reportType = this.reportComponent.reportType;
    this.reportDateFromTitle = 'Date From';
    this.reportDateToTitle = 'Date To';
    this.searchParamMap['reportFromDate'] = 'Date From';
    this.searchParamMap['reportToDate'] = 'Date To';
  }

  getReportData(data, summaryText) {
    let url = '';
    if (this.reportType  === 'productMaster') {
      url = PRODUCT_MASTER_REPORT + data;
    } else if (this.reportType  === 'productDetail') {
      url = PRODUCT_DETAIL_REPORT + data;
    } else if (this.reportType  === 'inventoryTransactions') {
      url = INVENTORY_TRANSACTION_REPORT + data;
    } else if (this.reportType  === 'deskReport') {
      url = DESK_REPORT + data;
    }
    const headers = this.fs.getHttpHeaders();
    this.http.get<Array<any>>(url, {
      withCredentials: true,
      headers: headers
    })
      .subscribe(reportReturndata => {
          const returnVal = {'data': reportReturndata, 'summaryText': summaryText};
          this.itemFound.emit(returnVal);
          this.appComponent.loader(false);
        },
        err => {
          this.appComponent.loader(false);
        });
  }

  createSearchCriteriaForm(fb: FormBuilder) {
    this.searchForm = fb.group({
      'productMasterId': '',
      'reportFromDate': this.utils.getFirstDayOfCurrMonth(),
      'reportToDate': new Date(),
      'showOnlyLatest': 'N',
      'inventoryStatus': 'ALL'
    });
  }

  retrieveAllProductMaster() {
    this.productMasterService.fetchAllProductMasterData().subscribe(data => {
      if (data.length > 0) {
        Object.keys(data).forEach((element) => {
          this.dropProductMasterList.push({'id': data[element].id, 'itemName': data[element].name});
        });
        this.dropProductMasterList.sort((a, b) => (a.itemName > b.itemName) ? 1 : -1);
      }
    }, err => {
      this.toastr.warningToastr(err['statusText'], 'Alert!', { position: 'bottom-right' });
      this.appComponent.loader(false);
    });
  }

  submitForm(postData) {
    this.appComponent.loader(true);
    let searchData = '';
    let summaryText = '';
    for (const field of Object.keys(postData)) {
      if (postData[field]) {
        if (summaryText !== '') { summaryText = summaryText + ', '; }
        if (field.includes('Date')) {
          summaryText = summaryText + this.searchParamMap[field] + ': ' +
            this.datepipe.transform(postData[field], this.globals.dateFormat, this.globals.timeZone);
          postData[field] = this.datepipe.transform(postData[field], this.globals.dateFormat, this.globals.timeZone);
        } else {
          summaryText = summaryText + this.searchParamMap[field] + ': ' + postData[field];
        }
        searchData = searchData + field + '=' + postData[field] + '&';
      }
    }
    if (this.productMasterId.length > 0) {
      searchData += 'productMasterId=' + this.productMasterId[0]['id'];
      summaryText = summaryText  + 'Product Name: ' + this.productMasterId[0].itemName;
    }
    this.getReportData(searchData, summaryText);
    // this.appComponent.loader(false);
  }

  resetForm() {
    this.searchForm.reset();
    this.createSearchCriteriaForm(this.fb);
  }

  OnItemDeSelectProduct(item: any) {
    this.productMasterId = [];
    this.searchForm.controls.productMasterId.setValue('');
  }

}
