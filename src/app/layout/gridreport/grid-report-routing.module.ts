import {NgModule} from '@angular/core';
import {Router, RouterModule, Routes} from '@angular/router';
import {GridReportComponent} from './grid-report.component';

const routes: Routes = [
  {
    path: '',
    component: GridReportComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GridReportRoutingModule {
  constructor(private router: Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }
}
