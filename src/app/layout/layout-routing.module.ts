import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LayoutComponent} from './layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {path: '', redirectTo: 'welcome'},
      {path: 'welcome', loadChildren: './desk/starter-content/welcome-content.module#WelcomeContentModule'},
      {path: 'user', loadChildren: './user/user.module#UserModule'},
      {path: 'barcode', loadChildren: './barcode/barcode.module#BarCodeModule'},
      {path: 'productMaster', loadChildren: './product-master/product-master.module#ProductMasterModule'},
      {path: 'mastercategory', loadChildren: './mastercategory/mastercategory.module#MasterCategoryModule'},
      {path: 'desk', loadChildren: './desk-master/desk-master.module#DeskMasterModule'},
      {path: 'user-profile', loadChildren: './user-profile/user-profile.module#UserProfileModule'},
      {path: 'jobTransaction', loadChildren: './jobtransaction/jobtransaction.module#JobTransactionModule'},
      {path: 'productdetails', loadChildren: './productdetails/productdetails.module#ProductdetailsModule'},
      {path: 'productMasterReport/:reportType', loadChildren: './gridreport/grid-report.module#GridReportModule'},
      {path: 'productDetailReport/:reportType', loadChildren: './gridreport/grid-report.module#GridReportModule'},
      {path: 'inventoryTransactionsReport/:reportType', loadChildren: './gridreport/grid-report.module#GridReportModule'},
      {path: 'deskReport/:reportType', loadChildren: './gridreport/grid-report.module#GridReportModule'},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule {
}
