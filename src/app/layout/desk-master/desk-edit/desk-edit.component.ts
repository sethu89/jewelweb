import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Globals} from '../../../shared/global';
import {AppComponent} from '../../../app.component';
import {DeskForm} from 'src/app/model/desk';
import {DeskService} from '../../../shared/services/desk.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {PicklistService} from '../../../shared/services';

@Component({
    selector: 'app-desk-edit',
    templateUrl: './desk-edit.component.html',
    styleUrls: ['./desk-edit.component.scss']
})
export class DeskEditComponent implements OnInit {
// tslint:disable-next-line:no-input-rename
    @Input('deskId') deskId: number;
    deskEditForm: FormGroup;
    title: string;
    activeStatusMap = new Map<String, String>();
    deskTypesMap = new Map<String, String>();


    constructor(private fb: FormBuilder, public globals: Globals, public deskService: DeskService, private pick: PicklistService,
                public appComponent: AppComponent, public toastr: ToastrManager) {
        this.createDeskEditForm();
    }

    ngOnInit() {
        this.fetchPickListValues();
        if (this.deskId > 0) {
            this.retrieveDesk();
        }
    }

    createDeskEditForm() {
        this.deskEditForm = this.fb.group({
            name: [null],
            description: [null],
            deskType: [null],
            status: 'true',
        });
    }

    displayDeskEditForm(retrieved: DeskForm) {
        this.deskEditForm = this.fb.group({
            id: retrieved.id,
            name: retrieved.name,
            description: retrieved.description,
            deskType: retrieved.deskType,
            status: retrieved.status,
        });
    }

    retrieveDesk() {
        this.title = this.globals.editTitle;
        this.retrieveDeskById();
    }


    retrieveDeskById() {
        this.appComponent.loader(true);
        this.deskService.fetchDeskById(this.deskId).subscribe(data => {
                this.displayDeskEditForm(data);
            },
            err => {
                this.toastr.warningToastr(err['statusText'], 'Alert!', {position: 'bottom-right'});

            });
        this.appComponent.loader(false);
    }


    submitDesk(postDeskEditForm) {
        this.appComponent.loader(true);
        const searchData = 'name=' + postDeskEditForm.name;
        this.deskService.fetchAllDesk(searchData).subscribe(data => {
            if (data && data.length > 0) {
                if (postDeskEditForm.id && postDeskEditForm.id === data[0].id) {
                    this.saveDesk(postDeskEditForm);
                } else {
                    this.toastr.warningToastr(postDeskEditForm.name + this.globals.deskExists, 'Alert!', { position: 'bottom-right' });
                    this.appComponent.loader(false);
                }
            } else {
                this.saveDesk(postDeskEditForm);
            }
        });
    }

    saveDesk(postDeskEdit) {
        this.appComponent.loader(true);
        if (this.deskId > 0) {
            postDeskEdit.id = this.deskId;
        }
        this.deskService.saveDesk(postDeskEdit).subscribe(data => {
                if (this.deskId > 0) {
                    this.toastr.successToastr(this.globals.deskUpdatedMessage, 'Success!', { position: 'bottom-right' });
                } else {
                    this.toastr.successToastr(this.globals.deskAddedMessage, 'Success!', { position: 'bottom-right' });
                    this.deskEditForm.reset();
                }
            },
            err => {
                this.toastr.warningToastr(err['statusText'], 'Alert!', { position: 'bottom-right' });
            });
        this.appComponent.loader(false);
    }

    fetchPickListValues() {
        this.pick.fetchPickListValues().subscribe(dataReturned => {
            const loc = <Map<String, Map<String, String>>>dataReturned;
            this.activeStatusMap = loc.get('ACTIVE_STATUS');
            this.deskTypesMap = loc.get('DESK_TYPES');
        });
    }

}
