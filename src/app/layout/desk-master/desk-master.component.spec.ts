import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeskMasterComponent } from './desk-master.component';

describe('ProductMasterComponent', () => {
  let component: DeskMasterComponent;
  let fixture: ComponentFixture<DeskMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeskMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeskMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
