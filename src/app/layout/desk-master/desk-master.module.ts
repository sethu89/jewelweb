import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { Globals } from '../../shared/global';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {UserService} from '../../shared/services/user.service';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {DeskMasterRoutingModule} from './desk-master-routing.module';
import {DeskMasterComponent} from './desk-master.component';
import {DeskEditComponent } from './desk-edit/desk-edit.component';
import {DeskSearchComponent } from './desk-search/desk-search.component';
import {DeskService} from '../../shared/services/desk.service';
import {PicklistService} from '../../shared/services';


@NgModule({
    declarations: [
        DeskMasterComponent,
        DeskSearchComponent,
        DeskEditComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        ReactiveFormsModule,
        DeskMasterRoutingModule,
        FormsModule, BsDatepickerModule.forRoot(), AngularMultiSelectModule,
    ],

    entryComponents: [DeskEditComponent],
    bootstrap: [DeskEditComponent],
    providers: [DeskEditComponent, Globals, DeskService, PicklistService],
})
export class DeskMasterModule {
}
