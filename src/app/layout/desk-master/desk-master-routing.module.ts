import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { DeskMasterComponent } from './desk-master.component';


const routes: Routes = [
    {
        path: '',
        component: DeskMasterComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DeskMasterRoutingModule {}
