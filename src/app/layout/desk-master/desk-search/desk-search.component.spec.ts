import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeskSearchComponent } from './desk-search.component';

describe('DeskSearchComponent', () => {
  let component: DeskSearchComponent;
  let fixture: ComponentFixture<DeskSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeskSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeskSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
