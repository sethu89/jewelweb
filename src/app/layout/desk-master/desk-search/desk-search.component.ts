import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AppComponent} from '../../../app.component';
import {ToastrManager} from 'ng6-toastr-notifications';
import {DeskService} from '../../../shared/services/desk.service';
import {Globals} from '../../../shared/global';
import {PicklistService} from '../../../shared/services';

@Component({
  selector: 'app-desk-search',
  templateUrl: './desk-search.component.html',
  styleUrls: ['./desk-search.component.scss']
})
export class DeskSearchComponent implements OnInit {
  @Output() itemFound = new EventEmitter<number>();
  deskSearchForm: FormGroup;
  showDeskSearchResult = false;
  deskSearchResult = [];
  deskTypesMap = new Map<String, String>();

  constructor(private fb: FormBuilder, public appComponent: AppComponent, public toastr: ToastrManager, public deskService: DeskService,
              public globals: Globals, private pick: PicklistService) { }

  ngOnInit() {
    this.createDeskSearchForm();
    this.fetchPickListValues();
  }

  createDeskSearchForm() {
    this.deskSearchForm = this.fb.group({
      name: [null],
    });
  }

  submitDeskForm(deskPostData) {
    this.appComponent.loader(true);
    let searchData = '';
    for (const field of Object.keys(deskPostData)) {
      if (deskPostData[field]) {
        searchData = searchData + field + '=' + deskPostData[field] + '&';
      }
    }
    this.getDataFromDeskService(searchData);
    this.appComponent.loader(false);
  }

  getDataFromDeskService(deskPostData) {
    this.appComponent.loader(true);
    this.deskService.fetchAllDesk(deskPostData).subscribe(data => {
      if (data.length > 0) {
        this.showDeskSearchResult = true;
        this.deskSearchResult = data;
        this.toastr.successToastr(data.length + this.globals.deskSearchMessage, 'Success!', { position: 'bottom-right' });
      } else {
        this.showDeskSearchResult = false;
        this.toastr.infoToastr(this.globals.noDeskMessage, 'Info!', { position: 'bottom-right' });
      }
      this.appComponent.loader(false);
    });
  }

  onAddDesk() {
    this.itemFound.emit(0);
  }
  onViewDesk(deskId) {
    this.itemFound.emit(deskId);
  }

  fetchPickListValues() {
    this.pick.fetchPickListValues().subscribe(dataReturned => {
      const loc = <Map<String, Map<String, String>>>dataReturned;
      this.deskTypesMap = loc.get('DESK_TYPES');
    });
  }

}
