import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-desk-master',
  templateUrl: './desk-master.component.html',
  styleUrls: ['./desk-master.component.scss']
})
export class DeskMasterComponent implements OnInit {
  showSearch: boolean;
  deskId: number;

  constructor() {
    this.showSearch = true;
  }

  ngOnInit() {
  }

  hideSearch(deskId: number) {
    this.deskId = deskId;
    this.showSearch = false;
  }

}
