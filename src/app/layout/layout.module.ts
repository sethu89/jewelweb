import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { StarterHeaderComponent } from './desk/starter-header/starter-header.component';
import { StarterFooterComponent } from './desk/starter-footer/starter-footer.component';
import { StarterLeftSideComponent } from './desk/starter-left-side/starter-left-side.component';
import { StarterControlSidebarComponent } from './desk/starter-control-sidebar/starter-control-sidebar.component';
import { NgxLoadingModule } from 'ngx-loading';



@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    NgxLoadingModule.forRoot({})
  ],
  declarations: [LayoutComponent,
    StarterHeaderComponent,
    StarterFooterComponent,
    StarterLeftSideComponent,
    StarterControlSidebarComponent,

  ]
})
export class LayoutModule {
}
