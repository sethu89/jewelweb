import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mastercategory',
  templateUrl: './mastercategory.component.html',
  styleUrls: ['./mastercategory.component.scss']
})
export class MastercategoryComponent implements OnInit {
  showSearch: boolean;
  masterCategoryId: number;
  constructor() {
    this.showSearch = true;

  }

  ngOnInit() {
  }
  hideSearch(masterCategoryId: number) {
    this.masterCategoryId = masterCategoryId;
    this.showSearch = false;
  }
}
