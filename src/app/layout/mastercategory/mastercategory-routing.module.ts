import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MastercategoryComponent} from './mastercategory.component';


const routes: Routes = [
    {
        path: '',
        component: MastercategoryComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MasterCategoryRoutingModule {}
