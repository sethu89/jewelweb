import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterCategoryRoutingModule } from './mastercategory-routing.module';
import { MastercategoryComponent } from './mastercategory.component';
import { ReactiveFormsModule } from '@angular/forms';

import { MasterdataComponent } from './masterdata/masterdata.component';
import { MastercategorySearchComponent } from './mastercategory-search/mastercategory-search.component';
import { MastercategoryEditComponent } from './mastercategory-edit/mastercategory-edit.component';
import {MasterCategoryService} from '../../shared/services/mastercategory.service';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {MasterService} from '../../shared/services/masterdata.service';
import {Globals} from '../../shared/global';


@NgModule({
  declarations: [
    MastercategoryComponent,
    MasterdataComponent,
    MastercategorySearchComponent,
    MastercategoryEditComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MasterCategoryRoutingModule, BsDatepickerModule.forRoot(),
  ],

  entryComponents: [MastercategoryEditComponent],
  bootstrap: [MastercategoryEditComponent],
  providers: [MastercategoryEditComponent, MasterCategoryService, MasterService, Globals],
})
export class MasterCategoryModule {
}
