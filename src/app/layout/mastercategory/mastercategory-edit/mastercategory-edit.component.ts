import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MasterCategoryService } from '../../../shared/services/mastercategory.service';
import { MasterCategory } from '../../../model/mastercategory';
import { AppComponent } from '../../../app.component';
import {ToastrManager} from 'ng6-toastr-notifications';
import { Globals } from '../../../shared/global';

@Component({
  selector: 'app-mastercategory-edit',
  templateUrl: './mastercategory-edit.component.html',
  styleUrls: ['./mastercategory-edit.component.scss']
})
export class MastercategoryEditComponent implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('masterCategoryId') masterCategoryIdEdited: number;
  masterCategoryEditForm: FormGroup;
  title: String;
  showSuccessAlert: boolean;
  showWarningAlert: boolean;
  message: string;
  showTabs = false;
  sampleList = ['Y', 'N'];


  masterDataDetailsTab: string[] = ['MasterData'];
  selectedTab = this.masterDataDetailsTab[0];
  constructor(private fb: FormBuilder, private mService: MasterCategoryService, public appComponent: AppComponent,
    public globals: Globals, public toastr: ToastrManager) {
    this.createEditForm(this.fb);

  }
  ngOnInit() {
    this.createNewMasterCategory();
    if (this.masterCategoryIdEdited > 0) {
      this.retrieve();
      this.masterCategoryEditForm.controls['category'].disable();
    }
    this.masterDataDetailsTab = ['MasterData'];
    this.selectedTab = this.masterDataDetailsTab[0];
  }

  createNewMasterCategory() {
    this.title = 'Create';
    this.createEditForm(this.fb);
  }

  createEditForm(fb: FormBuilder) {
    this.masterCategoryEditForm = fb.group({
      category: [null],
      picklist: 'Y',
    });
  }
  displayRetrievedMasterCategoryForm(fb: FormBuilder, retrieved: MasterCategory) {
    this.masterCategoryEditForm = fb.group({
      category: retrieved.category,
      picklist: retrieved.picklist,
      id: retrieved.id
    });
  }
  retrieve() {
    this.title = 'Edit';
    this.retrieveMasterCategory();
  }
  retrieveMasterCategory() {
    this.appComponent.loader(true);
    this.mService.fetchMasterCategoryById(this.masterCategoryIdEdited).subscribe(data => {
      this.displayRetrievedMasterCategoryForm(this.fb, data); this.showTabs = true;
    },
      err => {
        this.toastr.warningToastr(err['statusText'], 'Alert!', {position: 'bottom-right'});
      });
    this.appComponent.loader(false);
  }

  duplicateCheck(postData) {
    const searchData = 'category=' + postData.category  + '&exactMatch=' + true ;
    this.mService.searchMasterCategory(searchData).subscribe(data => {
      if (data && data.length > 0) {
        if (postData.id && postData.id === data[0].id) {
          this.submitMasterCategoryEditForm(postData);
        } else {
          this.toastr.infoToastr(postData.category + this.globals.categoryExists, 'Info!', {position: 'bottom-right'});
        }
      } else {
        this.submitMasterCategoryEditForm(postData);
      }
    });
  }

  submitMasterCategoryEditForm(postData) {
    this.appComponent.loader(true);
    if (this.masterCategoryIdEdited > 0) {
      postData.id = this.masterCategoryIdEdited;
    }
    this.mService.saveMasterCategory(postData).subscribe(data => {
      this.showTabs = true;
      if (this.masterCategoryIdEdited > 0) {
        this.toastr.successToastr(this.globals.categoryUpdatedMsg, 'Success!', {position: 'bottom-right'});
      } else {
        this.toastr.successToastr(this.globals.categoryAddedMgs, 'Success!', {position: 'bottom-right'});
      }
      this.masterCategoryIdEdited = data['id'];
    });
    this.appComponent.loader(false);
  }
  onChangePicklist() {
    const val = this.masterCategoryEditForm.controls['picklist'].value;
    if (!((val === 'Y') || (val === 'N'))) {
      alert('Please enter Y or N');
      this.masterCategoryEditForm.controls['picklist'].setValue(null);
    }
  }


  emitMasterCategoryId() {
    return this.masterCategoryEditForm.controls.category.value;
  }
  closeAlert() {
    this.showSuccessAlert = false;
    this.showWarningAlert = false;
  }
}


