import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MastercategoryEditComponent } from './mastercategory-edit.component';

describe('MastercategoryEditComponent', () => {
  let component: MastercategoryEditComponent;
  let fixture: ComponentFixture<MastercategoryEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MastercategoryEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MastercategoryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
