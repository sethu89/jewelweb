import { Component, OnInit} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MasterService } from '../../../shared/services/masterdata.service';
import { AppComponent } from '../../../app.component';
import { MastercategoryEditComponent } from '../mastercategory-edit/mastercategory-edit.component';
import { MasterData } from 'src/app/model/masterdata';
import { Globals } from '../../../shared/global';
import {ToastrManager} from 'ng6-toastr-notifications';


@Component({
  selector: 'app-masterdata',
  templateUrl: './masterdata.component.html',
  styleUrls: ['./masterdata.component.scss']
})
export class MasterdataComponent implements OnInit {
  masterDataEditForm: FormGroup;
  title: String;
  message: string;
  showSuccessAlert: boolean;
  showWarningAlert: boolean;
  masterCategory: number;
  showMasterDataList = false;
  masterDataList: [];

  constructor(private fb: FormBuilder, public datepipe: DatePipe, private mService: MasterService,
    public globals: Globals, public appComponent: AppComponent, private mastercategoryEditComponent: MastercategoryEditComponent,
    public toastr: ToastrManager) {
    this.createEditForm(fb);
    this.masterCategory = mastercategoryEditComponent.emitMasterCategoryId();
  }

  ngOnInit() {
    this.retrieveMasterData();
  }


  createEditForm(fb: FormBuilder) {
    this.masterDataEditForm = fb.group({
      code: [null],
      description: [null],
      activeDt: [null],
      expireDt: [null],
      notes: [null],
      displaySeq: [null],
    });
  }
  displayMasterDataForm(retrieved: MasterData) {
    this.masterDataEditForm.controls.code.disable();
    this.masterDataEditForm = this.fb.group({
      code: retrieved.code,
      description: retrieved.description,
      activeDt: this.datepipe.transform(retrieved.activeDt, this.globals.dateFormatYMDHS, this.globals.timeZone),
      expireDt: this.datepipe.transform(retrieved.expireDt, this.globals.dateFormatYMDHS, this.globals.timeZone),
      notes: retrieved.notes,
      displaySeq: retrieved.displaySeq,
      id: retrieved.id
    });
  }
  retrieveMasterData() {
    this.appComponent.loader(true);
    this.mService.searchMasterData(this.masterCategory).subscribe(data => {
      this.masterDataList = data;
      this.showMasterDataTable();
    },
      err => {
        this.toastr.warningToastr(err['statusText'], 'Alert!', {position: 'bottom-right'});
      });
    this.appComponent.loader(false);
  }


  submitMasterDataEditForm(postData) {
    this.appComponent.loader(true);
    postData.activeDt = new Date(postData.activeDt).getTime();
    postData.expireDt = new Date(postData.expireDt).getTime();
    postData.isUserGenerated = 'Y';
    postData.category = this.masterCategory;
    this.appComponent.loader(true);
    if (this.masterCategory > 0) {
      postData.category = this.masterCategory;
    }
    this.mService.saveMasterData(postData).subscribe(data => {
      this.masterDataEditForm.controls.code.enable();
      if (postData.id > 0) {
        this.toastr.successToastr(this.globals.masterDataUpdatedMessage, 'Success!', {position: 'bottom-right'});
      } else {
        this.toastr.successToastr(this.globals.masterDataAddedMessage, 'Success!', {position: 'bottom-right'});
      }
      this.showMasterDataTable();
      this.retrieveMasterData();
      this.masterDataEditForm.reset();
    });
    this.appComponent.loader(false);
  }

  showMasterDataTable() {
    if (this.masterDataList.length) {
      this.showMasterDataList = true;
    } else {
      this.showMasterDataList = false;
    }
  }
  onViewMasterData(index) {
    this.displayMasterDataForm(this.masterDataList[index]);
    this.closeAlert();
  }

  closeAlert() {
    this.showSuccessAlert = false;
    this.showWarningAlert = false;
  }
}
