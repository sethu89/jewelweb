import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MastercategorySearchComponent } from './mastercategory-search.component';

describe('MastercategorySearchComponent', () => {
  let component: MastercategorySearchComponent;
  let fixture: ComponentFixture<MastercategorySearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MastercategorySearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MastercategorySearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
