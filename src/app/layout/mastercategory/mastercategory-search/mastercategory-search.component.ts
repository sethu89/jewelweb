import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MasterCategoryService } from '../../../shared/services/mastercategory.service';
import { AppComponent } from '../../../app.component';
import {Globals} from '../../../shared/global';
import {ToastrManager} from 'ng6-toastr-notifications';

@Component({
  selector: 'app-mastercategory-search',
  templateUrl: './mastercategory-search.component.html',
  styleUrls: ['./mastercategory-search.component.scss']
})
export class MastercategorySearchComponent implements OnInit {
  @Output() itemFound = new EventEmitter<number>();
  masterCategorySearchForm: FormGroup;
  showSearchResult = false;
  searchResult = [];
  showSuccessAlert: boolean;
  showWarningAlert: boolean;
  message: string;
  sampleList = ['Y', 'N'];
  exactMatch = false;
  toggleVisibility(e) {
    this.exactMatch = e.target.checked;
  }
  constructor(private fb: FormBuilder, private mService: MasterCategoryService ,  public appComponent: AppComponent,
    public globals: Globals, public toastr: ToastrManager) {
    this.createSearchForm(this.fb);

  }

  ngOnInit() {
  }
  createSearchForm(fb: FormBuilder) {
    this.masterCategorySearchForm = fb.group({
      category: [null],
      picklist: [null],
    });
  }
  submitMasterCategoryForm(postData) {
    let searchData = '';
    for (const field of Object.keys(postData)) {
      if (postData[field]) {
        searchData = searchData + field + '=' + encodeURIComponent(postData[field]) + '&' + 'exactMatch=' + this.exactMatch ;
      }
    }
    this.getDataToService(searchData);
    this.appComponent.loader(false);
  }
  getDataToService(postData) {
    this.appComponent.loader(true);
    this.mService.searchMasterCategory(postData).subscribe(data => {
      if (data.length > 0) {
        this.showSearchResult = true;
        this.searchResult = data;
        this.toastr.successToastr(data.length +  this.globals.masterCategorySearchMsg, 'Success!', {position: 'bottom-right'});
      } else {
        this.toastr.infoToastr(this.globals.masterCategoryNoDataMsg, 'Info!', {position: 'bottom-right'});
        this.showSearchResult = false;
      }
      this.appComponent.loader(false);
    });
  }
  onViewMasterCategory(masterCategoryId) {
    this.itemFound.emit(masterCategoryId);
  }

  onAddMasterCategory() {
    this.itemFound.emit(0);
  }

  ChangeRefresh() {
    this.mService.pickListRefresh().subscribe( refreshData => {
      this.toastr.successToastr(this.globals.categoryRefresh, 'Success!', {position: 'bottom-right'});
    },
      err => {
        this.toastr.warningToastr(err['statusText'], 'Alert!', {position: 'bottom-right'});
      });
  }

  onChangePicklist() {
    const val = this.masterCategorySearchForm.controls['picklist'].value;
    if (!((val === 'Y') || (val === 'N'))) {
      alert('Please enter Y or N');
      this.masterCategorySearchForm.controls['picklist'].setValue(null);
    }
  }

  closeAlert() {
    this.showSuccessAlert = false;
    this.showWarningAlert = false;
  }
}
