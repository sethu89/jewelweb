import {Component, OnInit} from '@angular/core';
import { Router} from '@angular/router';
import {AppComponent} from '../app.component';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  bodyClasses = 'skin-blue sidebar-mini';
  body: HTMLBodyElement = document.getElementsByTagName('body')[0];

  constructor(public router: Router, public appComponent: AppComponent) {
  }

  ngOnInit() {
    if (localStorage.getItem('isLoggedin') !== 'true') {
      this.router.navigate(['/login']);
      localStorage.setItem('isLoggedin', 'false');
    }
    // add the the body classes
    this.body.classList.add('skin-blue');
    this.body.classList.add('sidebar-mini');
  }


}
