import {CommonModule, DatePipe} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AuthGuard, MaterialModule} from './shared';
import {FactoryService} from './shared/services/factory.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PicklistService} from './shared/services';
import {CommonutilsService} from './shared/services/commonutils.service';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {GridReportService} from './shared/services/gridreport.service';

import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';

import { TranslateModule } from '@ngx-translate/core';
import { NgxLoadingModule } from 'ngx-loading';
import {Globals} from './shared/global';
import { ToastrModule } from 'ng6-toastr-notifications';
import { UserIdleModule } from 'angular-user-idle';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    AngularMultiSelectModule,
    TranslateModule.forRoot(),
    NgxLoadingModule.forRoot({}),
    ToastrModule.forRoot(),
    UserIdleModule.forRoot({idle: 1200, timeout: 60})
  ],
  declarations: [AppComponent],
  providers: [AuthGuard, FactoryService, PicklistService, DatePipe,
    CommonutilsService, GridReportService, Globals],
  bootstrap: [AppComponent]
})
export class AppModule {
}
