import {Injectable} from '@angular/core';
import {DatePipe} from '@angular/common';

@Injectable()
export class Globals {

  constructor( public datepipe: DatePipe) {}

  vendorId = '1';
  vendorTypeCode = 'V';
  GstinCode = 'GSTIN-CODE';
  IeCode = 'IE-CODE';
  vendor = 'V';
  responsiblePerson = 'R';
//// Part Tabs
  partDetailsTab = ['Child Part', 'Standard Ref', 'CSR(Notes)',
    'Revision Amendment', 'Stage & Status', 'HS Code', 'Image', 'Part Domain', 'Price', 'RFQ/MBQ', 'Supplier X Product'];
//// Part Tabs
  partDetailsTabAdmin = ['Child Part', 'Standard Ref', 'CSR(Notes)',
    'Revision Amendment', 'Stage & Status', 'HS Code', 'Image', 'Part Domain', 'Supplier X Product'];
  sampleList = ['LOT', 'SHIFT'];
  otherFrequencyList = ['LOT', 'SHIFT', 'SETTING'];
  sampleFrequencyMap = {'-222': 'LOT', '-333': 'SHIFT', '1': '100%'};
  controlPlanTab = ['Control Plan Process', 'Control Plan Item', 'Process Cycle Time'];
  supplierDetailsTab = ['Supplier X Product'];
  toolsSubTab = ['Tool X Insert', 'Tool X Holder', 'Supplier X Product'];
  shiftTab = ['Shift Timings'];
  lotValue = '-222';
  shiftValue = '-333';

  yOrNList = ['Y', 'N'];

  // Documents Type
  atsDocumentType = 'atsContentSheet';


  // Date Format
  dateFormatYMDHS = 'yyyy-MM-dd HH:mm:ss';
  dateFormatYMD = 'yyyy-MM-dd';
  dateFormat = 'yyyy-MM-dd';
  timeZone = 'IST';

  // Others
  // title
  editTitle = 'Edit';
  createTitle = 'Create';
  completed = 'Completed';
  approvedStatusCode = 'APV';
  reworkStatusCode = 'RW'; // Like given in codes table
  machiningRejection = 'MRJ';
  castingRejection = 'CRJ';
  newStatus = 'NEW';
  placed = 'PLACED'; // For supplier Po
  parReceiveStat = 'PARTIAL RECEIVED'; // For supplier Po
  receivedStat = 'RECEIVED'; // For supplier Po
  holdStatus = 'HOLD';
  hold = 'H'; // for hold in task
  rejecetedStatus = 'RJ';
  warningColor = 'red';
  pending = 'Pending';

  rework = 'Rework';
  go = 'Go';
  noGo = 'NoGo';

  open = 'OPE';

  closeStatus = 'CLS'; // idle time Closed Status.

  // task Validation tab heading
  titleRework = 'Rework';
  titlePending = 'Pending';


  // Task Type these codes in the Codes table
  taskTypeRework = 'RW';
  taskTypeInspection = 'INS';
  taskTypetask = 'TK';

  expInvoice = 'ExpInvoice';
  toolInvoice = 'ToolInvoice';

  toolInvoiceType = 'TOOL';
  nonToolInvoiceType = 'NONTOOL';


  //
  runningNumberFor100Percent = 23;   // Dummy value to get 100 percent frequency alse for the otherfrequency
  // Error
  error = 'Error';

  // download unauthorized
  unauthDownload = 'Unauthorized Access To Download';

  // Desk
  deskAddedMessage = 'Desk Added Successfully';
  deskUpdatedMessage = 'Desk Updated Successfully';
  deskSearchMessage  = ' Desk(s) Retrieved.';
  deskExists = ' - Desk already exists';

  noDeskMessage = 'No Desk(s) Retrieved';

  // Master Category
  masterCategorySearchMsg = ' Master Category(s) Retrieved.';
  categoryRefresh = 'Category Refresh Successfully';
  masterCategoryNoDataMsg = 'No  Master Category(s) Retrieved for your Input';
  categoryExists = ' - Category already exists';
  categoryUpdatedMsg = 'Master Category  updated successfully';
  categoryAddedMgs = 'Master Category added successfully';

  // Master Data
  masterDataAddedMessage = 'Master Data added successfully';
  masterDataUpdatedMessage = 'Master Data  updated successfully';


  // Salary
  salaryAddedMessage = 'Salary Details Added';
  salaryUpdatedMessage = 'Salary Details Updated';

  dateConfig: any = {
    dateInputFormat: 'YYYY-MM-DD',
    containerClass: 'theme-blue'
  };

  // Product Master
  productMasterSearchMsg = ' Product Master(s) Retrieved.';
  productMasterNoDataMsg = 'No Product Master(s) Retrieved';
  productMasterAddedMsg = 'Product Master added successfully';
  productMasterUpdatedMsg = 'Product Master  updated successfully';
  productMasterAlreadyExistsMsg = ' Product Master  Already Exists';
  productBarcodeAlreadyExistsMsg = ' Product Barcode  Already Exists';

  // User
  userNameExist = ' - userName already exists';
  userUpdateMsg = 'User  updated successfully';
  userAddedMsg = 'User added successfully';
  userNameExists = ' - First Name and Last Name already exists';
  userRetrieved = ' User(s) Retrieved.';
  noUserRetrieved = 'No User(s) Retrieved for your Input';

  // reset-password
  passwordUpdateMsg = 'Password  updated successfully';


  // BarCode
  barcodeRangeAlertMessage  = 'Given From Number is Greater than To Number.';

  // Job Transaction
  jobTransactionSearchMsg = ' Job Transaction(s) Retrieved.';
  jobTransactionNoDataMsg = 'No Job Transaction(s) Retrieved for your Input';
  jobTransactionAddedMsg = 'Job Transaction added successfully';
  jobTransactionUpdatedMsg = 'Job Transaction  updated successfully';

  multiSelectItem(id, itemName) {
    const val: { id: String, itemName: String } = {id: '1', itemName: 'name'};
    val.id = id;
    val.itemName = itemName;
    return val;
  }

  dropdownSettings(selectText: String, singleSelection: boolean) {
    const dropdownPoSettings = {     // settings for multiselect dropdown
      singleSelection: singleSelection,
      text: selectText,
      enableSearchFilter: true,
      classes: 'myclass custom-class',
      unSelectAllText: 'UnSelect All',
      selectAllText: 'Select All',
    };
    return dropdownPoSettings;
  }

  enableDropdownSettings(selectText: String, singleSelection: boolean) {
    const dropdownPoSettings = {     // settings for multiselect dropdown
      singleSelection: singleSelection,
      text: selectText,
      enableSearchFilter: true,
      classes: 'myclass custom-class',
      unSelectAllText: 'UnSelect All',
      selectAllText: 'Select All',
      disabled : false
    };
    return dropdownPoSettings;
  }

  disableDropDownSetting(selectText: String, singleSelection: boolean) {
    const dropdownSettings = {     // settings for multiselect dropdown
      singleSelection: singleSelection,
      text: selectText,
      enableSearchFilter: true,
      classes: 'myclass custom-class',
      unSelectAllText: 'UnSelect All',
      selectAllText: 'Select All',
      disabled : true
    };
    return dropdownSettings;
  }

  datesFromTheMonthAndYr(month, year) {
    const dates = [];
    const startDate = this.datepipe.transform(new Date(year, month - 1, 1), this.dateFormatYMDHS, this.timeZone);
    const endDate = this.datepipe.transform(new Date(year, month, 0), this.dateFormatYMDHS, this.timeZone);

    // const transStartDate = this.datepipe.transform(startDate, this.dateFormatYMDHS, this.timeZone);
    // const transEndDate = this.datepipe.transform(endDate, this.dateFormatYMDHS, this.timeZone);

    dates['startDate'] = startDate;
    dates['endDate'] = endDate;

    return  dates;
  }

  datesFromTheMonthAndYrInYMD(month, year) {
    const dates = [];
    const startDate = this.datepipe.transform(new Date(year, month - 1, 1), this.dateFormatYMD, this.timeZone);
    const endDate = this.datepipe.transform(new Date(year, month, 0), this.dateFormatYMD, this.timeZone);
    dates['startDate'] = startDate;
    dates['endDate'] = endDate;

    return  dates;
  }

  retrieveScheduleYearList() {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const scheduleYearList = new Map<String, String>();

    scheduleYearList.set(String((currentYear) - 1), String((currentYear) - 1));
    scheduleYearList.set(String(currentYear), String(currentYear));
    scheduleYearList.set(String((currentYear) + 1), String((currentYear) + 1));
    scheduleYearList.set(String((currentYear) + 2), String((currentYear) + 2));

    return scheduleYearList;
  }

  addressFormat(data) {
    if (data) {
      let address  = '';
      if (data.lineOne) {
        address = data.lineOne;
      }
      if (data.lineTwo) {
        address = address + ', ' + data.lineTwo;
      }
      if (data.lineThree) {
        address = address + ', ' + data.lineThree;
      }
      if (data.lineFour) {
        address = address + ', ' + data.lineThree;
      }
      address = address + data.cityName + ', ' +
        data.provinceName + ', ' + data.countryName + ' - ' + data.postalCd;
      return address;

    }
  }
}
