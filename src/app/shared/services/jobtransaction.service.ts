// @ts-ignore
import { JOBTRANSACTION_SAVE, JOBTRANSACTION_SEARCH_ALL, JOBTRANSACTION_SEARCH_BYID } from './endpoints';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { FactoryService } from './factory.service';
import { Observable } from 'rxjs';
import { FactoryService } from '.';


@Injectable()
export class JobTransactionService {

    constructor(private http: HttpClient, private fs: FactoryService, ) {
    }
    fetchJobTransactionById(jobTransactionId: number): Observable<any> {
        const url = JOBTRANSACTION_SEARCH_BYID + jobTransactionId;
        return Observable.create(observer => {
            this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(data => {
                    observer.next(data);
                    observer.complete();
                });
        });
    }

    saveJobTransaction(toSavejobTransactiondata): Observable<any> {
        const url = JOBTRANSACTION_SAVE;
        return Observable.create(observer => {
            this.http.post(url, toSavejobTransactiondata, this.fs.getHttpOptions())
                .subscribe(contentData => {
                    observer.next(contentData);
                    observer.complete();
                });
        });
    }

    fetchAllJobTransaction(postData): Observable<any> {
        const url = JOBTRANSACTION_SEARCH_ALL + postData ;
        return Observable.create(observer => {
            this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(data => {
                    observer.next(data);
                    observer.complete();
                });
        });
    }
}
