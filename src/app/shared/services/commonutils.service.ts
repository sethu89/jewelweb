import {Injectable} from '@angular/core';
import {FactoryService} from './factory.service';
import htmlToImage from 'html-to-image';
import {DatePipe} from '@angular/common';
import {AppComponent} from '../../app.component';
import {Globals} from '../global';

@Injectable()
export class CommonutilsService {
  constructor(public datepipe: DatePipe, public globals: Globals) {
  }

  getUTCDate(dateVal): Date {
    const date = new Date(dateVal);
    const formattedDate =  new Date(date.getUTCFullYear(),
      date.getUTCMonth(),
      date.getUTCDate(),
      date.getUTCHours(),
      date.getUTCMinutes(),
      date.getUTCSeconds());
    date.setHours(date.getHours() - date.getTimezoneOffset() / 60);
    return date;
  }

  getFirstDayOfCurrFY() {
    const today = new Date();
    // get current month
    const curMonth = today.getMonth();
    let fiscalYr = '';
    if (curMonth >= 3) { //
      fiscalYr = today.getFullYear().toString() + '-04-01' ;
    } else {
      fiscalYr = (today.getFullYear() - 1).toString() + '-04-01';
    }
    return fiscalYr;
  }

  getFirstDayOfCurrMonth() {
    const today = new Date();
    // get current month
    const curMonth = today.getMonth() + 1;
    return  today.getFullYear().toString() + '-' + curMonth + '-01' ;
  }

  getLastDayOfCurrMonth() {
    const today = new Date();
    // get current month
    const curMonth = today.getMonth() + 1;
    // return  today.getFullYear().toString() + '-' + curMonth + '-01' ;
    return this.datepipe.transform(new Date(today.getFullYear(), curMonth, 0),  this.globals.dateFormatYMDHS, this.globals.timeZone);

  }

  getDivImage(divElementId, imageName) {
    const htmlToImage1 = htmlToImage;
    htmlToImage1.toPng(document.getElementById(divElementId))
      .then(function (dataUrl) {
        const link = document.createElement('a');
        link.download = imageName;
        link.href = dataUrl;
        link.click();
        link.remove();
      })
      .catch(function (error) {
        console.error('oops, something went wrong!', error);
      });
  }
}
