// @ts-ignore
import {GENERATE_BARCODE} from './endpoints';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FactoryService } from '.';


@Injectable()
export class BarCodeService {

  constructor(private http: HttpClient, private fs: FactoryService, ) {
  }

  generateBarCode(serialNo, fromNumber, noOfLables): Observable<any> {
    const url = GENERATE_BARCODE + '?serialNo=' + serialNo + '&fromNumber=' + fromNumber + '&noOfLables=' + noOfLables;
      const headers = this.fs.getPdfHttpHeaders();
    return Observable.create(observer => {
      this.http.get(url, {responseType: 'blob', headers: headers, withCredentials: true} )
        .subscribe(contentData => {
          observer.next(contentData);
          observer.complete();
        });
    });




  //   // this.appComponent.loader(true);
  //   const url = GENERATE_BARCODE + invoiceId + '&invoiceType=' + invoiceType;
  //   const headers = this.fs.getPdfHttpHeaders();
  //   this.http.get(url, {responseType: 'blob', headers: headers, withCredentials: true})
  //       .subscribe(data => {
  //               const blob = new Blob([data], {type: 'application/pdf'});
  //             saveAs(blob, `${invoiceNumber}.pdf`);
  //             this.appComponent.loader(false);
  //           },
  //           err => {
  //             this.appComponent.loader(false);
  //             alert('Error ' + err['statusText']);
  //           }
  //       );
  }
}
