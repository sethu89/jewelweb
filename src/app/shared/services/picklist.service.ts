import {Injectable} from '@angular/core';
import {Codes} from '../../model/codes';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PICK_LIST} from './endpoints';
import {FactoryService} from './factory.service';

@Injectable()
export class PicklistService {
  constructor(private http: HttpClient, private fs: FactoryService) {
  }

  picklistValues = new Map<String, Map<String, String>>();

  fetchPickListValues(): Observable<Map<String, Map<String, String>>> {
    const url = PICK_LIST;
    const headers = this.fs.getHttpHeaders();
    return Observable.create(observer => {
      this.http.get<Map<String, Map<String, Codes>>>(url, {
        withCredentials: true,
        headers: headers
      })
        .subscribe(data => {
          this.extractPickList(data);
          observer.next(this.picklistValues);
          observer.complete();
        });
    });
  }

  extractPickList(data: Map<String, Map<String, Codes>>) {
    // Typescript weird ways to make sure the structure is recreated
    const loc = new Map<String, Map<String, Codes>>(Object.entries(data));
    loc.forEach((v: Map<String, Codes>, k: String) => {
      const valueMap = new Map<String, String>();
      const tempMap = new Map<String, Codes>(Object.entries(v));
      tempMap.forEach((mv: Codes, mk: String) => {
        valueMap.set(mk, mv.description);
      });
      this.picklistValues.set(k, valueMap);
    });
  }
}
