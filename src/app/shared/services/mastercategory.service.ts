import { MASTERCATEGORY_SEARCH , MASTERCATEGORY_SAVE , MASTERCATEGORY_SEARCH_BYID, PICKLIST_REFRESH } from './endpoints';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FactoryService} from './factory.service';
import {Observable} from 'rxjs';

@Injectable()
export class MasterCategoryService {
    constructor(private http: HttpClient, private fs: FactoryService) {
         }

         fetchMasterCategoryById(masterCategoryId: number): Observable<any> {
            const url = MASTERCATEGORY_SEARCH_BYID + masterCategoryId;
            return Observable.create(observer => {
              this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(data => {
                  observer.next(data);
                  observer.complete();
                });
            });
          }
          saveMasterCategory(toSaveMasterCategorydata): Observable<any> {
            const url = MASTERCATEGORY_SAVE;
            return Observable.create(observer => {
              this.http.post(url, toSaveMasterCategorydata, this.fs.getHttpOptions())
                .subscribe(contentData => {
                  observer.next(contentData);
                  observer.complete();
                });
            });
          }
          searchMasterCategory(postData): Observable<any> {
            const url = MASTERCATEGORY_SEARCH + postData;
            return Observable.create(observer => {
              this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(data => {
                  observer.next(data);
                  observer.complete();
                });
            });
          }
          pickListRefresh(): Observable<any> {
            const url = PICKLIST_REFRESH;
            return Observable.create(observer => {
              this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(data => {
                  observer.next(data);
                  observer.complete();
                });
            });
          }
}
