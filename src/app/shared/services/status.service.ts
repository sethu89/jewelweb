import {Injectable} from '@angular/core';

@Injectable()
export class StatusService {
  public statusMap = new Map<String, Map<String, String>>();
  public specValMap = new Map<String, Map<Number, String>>();
  updateMap = new Map<String, String>();
  appliedMap = new Map<String, String>();
  sentMap = new Map<String, String>();
  yesNoMap = new Map<String, String>();
  yesNoSingleWordMap = new Map<String, String>();
  reviceMap = new Map<String, String>();
  goNoGoMap = new Map<Number, String>();

  statusList() {
    this.updateMap.set('Updated', 'Updated');
    this.updateMap.set('Not Update', 'Not Update');
    this.updateMap.set('NA', 'NA');

    this.appliedMap.set('Applied', 'Applied');
    this.appliedMap.set('Not Applied', 'Not Applied');
    this.appliedMap.set('NA', 'NA');

    this.sentMap.set('Sent', 'Sent');
    this.sentMap.set('Not sent', 'Not sent');
    this.sentMap.set('NA', 'NA');

    this.yesNoMap.set('yes', 'yes');
    this.yesNoMap.set('No', 'No');
    this.yesNoMap.set('NA', 'NA');

    this.yesNoSingleWordMap.set('Y', 'yes');
    this.yesNoSingleWordMap.set('N', 'No');
    this.yesNoSingleWordMap.set('A', 'NA');
    this.reviceMap.set('Received', 'Received');
    this.reviceMap.set('Not Received', 'Not Received');
    this.reviceMap.set('NA', 'NA');

    this.statusMap.set('EDPMSATTR1', this.updateMap);
    this.statusMap.set('MEISATTR1', this.appliedMap);
    this.statusMap.set('INVCUATTR2', this.sentMap);
    this.statusMap.set('MEISATTR2', this.reviceMap);
    this.statusMap.set('PRATTR3', this.reviceMap);
    this.statusMap.set('IGRCATTR3', this.reviceMap);
    this.statusMap.set('DDBATTR3', this.reviceMap);
    this.statusMap.set('SBRATTR4', this.yesNoMap);
    this.statusMap.set('AWBBLATTR2', this.yesNoMap);
    this.statusMap.set('FIRCATTR3', this.reviceMap);
    // for LUt in invoice status
    this.statusMap.set('LUT', this.yesNoSingleWordMap);
    return this.statusMap;
  }
  specList () {
    this.goNoGoMap.set(1, 'Go/ok');
    this.goNoGoMap.set(0, 'NoGo/Not Ok');
    this.specValMap.set('specVal', this.goNoGoMap);
    return this.specValMap;
  }
}
