// @ts-ignore
import {DESK_SAVE, DESK_SEARCH_ALL, DESK_SEARCH_BYID, PRODUCT_MASTER_LIST} from './endpoints';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { FactoryService } from './factory.service';
import { Observable } from 'rxjs';
import { FactoryService } from '.';


@Injectable()
export class DeskService {

    constructor(private http: HttpClient, private fs: FactoryService, ) {
    }

    fetchAllDeskList(): Observable<any> {
        const url = DESK_SEARCH_ALL;
        const deskMap = new Map<String, String>();
        const data = [];
        const deskList = [];
        return Observable.create(observer => {
            this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(returnData => {
                    returnData.forEach(item => {
                        const val: { id: String, itemName: string } = { id: '1', itemName: 'name' };
                        val.id = item.id;
                        val.itemName = item.name;
                        deskList.push(val);
                        deskMap.set(val.id, val.itemName);
                    });
                    data['deskList'] = deskList;
                    data['deskMap'] = deskMap;
                    observer.next(data);
                    observer.complete();
                });
        });
    }
    fetchDeskById(deskId: number): Observable<any> {
        const url = DESK_SEARCH_BYID + deskId;
        return Observable.create(observer => {
            this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(data => {
                    observer.next(data);
                    observer.complete();
                });
        });
    }

    saveDesk(toSaveDeskdata): Observable<any> {
        const url = DESK_SAVE;
        return Observable.create(observer => {
            this.http.post(url, toSaveDeskdata, this.fs.getHttpOptions())
                .subscribe(contentData => {
                    observer.next(contentData);
                    observer.complete();
                });
        });
    }

    fetchAllDesk(postData): Observable<any> {
        const url = DESK_SEARCH_ALL + postData ;
        return Observable.create(observer => {
            this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(data => {
                    observer.next(data);
                    observer.complete();
                });
        });
    }
}
