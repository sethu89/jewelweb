import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { UPLOAD_FILE} from '../services/endpoints';
import {FactoryService} from './factory.service';


@Injectable({
  providedIn: 'root'
})
export class UploadService {
  constructor(private httpClient: HttpClient, private fs: FactoryService) { }

  public upload(data) {
    const uploadURL =  UPLOAD_FILE;
    return this.httpClient.post<any>(uploadURL, data,  {
      reportProgress: true,
      observe: 'events',
      withCredentials: true,
      headers: this.fs.getHttpHeadersAuth()
    }).pipe(map((event) => {

        switch (event.type) {

          case HttpEventType.UploadProgress:
            const progress = Math.round(100 * event.loaded / event.total);
            return { status: 'progress', message: progress };

          case HttpEventType.Response:
            return event.body;
          default:
            return `Unhandled event: ${event.type}`;
        }
      })
    );
  }
}
