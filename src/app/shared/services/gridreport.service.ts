import {Injectable} from '@angular/core';


@Injectable()
export class GridReportService {
  columns = [];

  constructor() {
  }

  public getGridColumns(reportType) {
    if (reportType === 'inventoryTransactions') {
      this.columns = [
        {title: 'Product', data: 'product'},
        {title: 'Bar Code', data: 'barCode'},
        {title: 'Status', data: 'overallStatus'},
        {title: 'User', data: 'user'},
        {title: 'Desk', data: 'desk'},
        {title: 'Date Time', data: 'inTime'},
        {title: 'Job Type', data: 'jobType'},
        {title: 'Remarks', data: 'remarks'},
      ];
    } else if (reportType === 'productMaster') {
      this.columns = [
        {title: 'Product', data: 'productName'},
        {title: 'Bar Code Char', data: 'barCodeChar'},
        {title: 'Last Generated Bar Code', data: 'lastBarCode'},
        {title: 'Total Count', data: 'barCodeCnt', total: true, className: 'text-right'},
        {title: 'Inactive Count', data: 'inactiveCnt', total: true, className: 'text-right'},
        {title: 'Active Count', data: 'activeCnt', total: true, className: 'text-right'},
        {title: 'Sold Count', data: 'soldCnt', total: true, className: 'text-right'},
        {title: 'Broken Count', data: 'brokenCnt', total: true, className: 'text-right'},
        {title: 'Stock Count', data: 'stockCnt', total: true, className: 'text-right'},
      ];
    } else if (reportType === 'productDetail') {
      this.columns = [
        {title: 'Product Name', data: 'productName'},
        {title: 'Bar Code', data: 'barCode'},
        {title: 'Current Status', data: 'currStatus'},
        {title: 'Status Date', data: 'statusDate'},
        {title: 'Customer Name', data: 'customerName'},
        {title: 'remarks', data: 'remarks'},
        {title: 'jobTransactionList', data: 'jobTransactionList', visible: false, searchable: false},
      ];
    } else if (reportType === 'deskReport') {
      this.columns = [
        {title: 'Desk Name', data: 'deskName'},
        {title: 'Product Name', data: 'productName'},
        {title: 'IN Count', data: 'inCnt', total: true, className: 'text-right'},
        {title: 'OUT Count', data: 'outCnt', total: true, className: 'text-right'},
        {title: 'Stone Count', data: 'stoneCnt', total: true, className: 'text-right'},
        {title: 'Total Weight', data: 'prodWeight', total: true, className: 'text-right'},
      ];
    }
    return this.columns;
  }
}
