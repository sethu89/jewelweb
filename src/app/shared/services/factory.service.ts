import {Injectable} from '@angular/core';
import {HttpHeaders} from '@angular/common/http';

@Injectable()
export class FactoryService {
  constructor() {
  }

  getHttpHeaders() {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json')
      .append('Authorization', 'Bearer ' + localStorage.getItem('X-QQ-Auth-token'));
    return headers;
  }

  getHttpHeadersAuth() {
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', 'Bearer ' + localStorage.getItem('X-QQ-Auth-token'));
    return headers;
  }

  getServHttpHeaders() {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json')
      .append('Authorization', 'Bearer ' + localStorage.getItem('X-QQ-Auth-token'));
    return headers;
  }

  getFileHttpHeaders() {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'multipart/form-data')
      .append('Authorization', 'Bearer ' + localStorage.getItem('X-QQ-Auth-token'));

    return headers;
  }

  getPdfHttpHeaders() {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/pdf')
      .append('Authorization', 'Bearer ' + localStorage.getItem('X-QQ-Auth-token'));
    return headers;
  }

  getPOPdfHttpHeaders() {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/pdf')
      .append('Authorization', 'Bearer ' + localStorage.getItem('X-QQ-Auth-token'));
    return headers;
  }

  makeQueryString(postData): string {
    let queryString = '';
    for (const field of Object.keys(postData)) {
      if (postData[field]) {
        queryString = queryString + field + '=' + postData[field] + '&';
      }
    }
    return queryString;
  }

  formatDate(date: string): string {
    return new Date(date).toLocaleString('en-US',
      {year: 'numeric', month: 'numeric', day: 'numeric'});
  }

  getHttpOptions(): object {
    const options = {
      withCredentials: true,
      headers: this.getHttpHeaders()
    };
    return options;
  }

  getServHttpOptions(): object {
    const options = {
      withCredentials: true,
      headers: this.getServHttpHeaders()
    };
    return options;
  }

  getUserRoles(): string {
    const roles = localStorage.getItem('roles');
    return roles;
  }

  isUserAdmin(): boolean {
    const roles = this.getUserRoles();
    if (roles != null && roles.indexOf('ADMIN', 0) > -1) {
      return true;
    }
    return false;
  }

  isUserManager(): boolean {
    const roles = this.getUserRoles();
    if (roles != null && roles.indexOf('MANAGER', 0) > -1) {
      return true;
    }
    return false;
  }

}
