import {USER_SAVE, USER_SEARCH, USER_SEARCH_BYID, SAVE_USER_LOGIN, USER_SEARCH_ALL} from './endpoints';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FactoryService } from './factory.service';
import { Observable } from 'rxjs';
import { PicklistService } from './picklist.service';

@Injectable()
export class UserService {
  constructor(private http: HttpClient, private fs: FactoryService, private pick: PicklistService) {
  }

  fetchUserById(userId: number): Observable<any> {
    const url = USER_SEARCH_BYID + userId;
    return Observable.create(observer => {
      this.http.get<any>(url, this.fs.getHttpOptions())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  saveUser(toSaveUserdata): Observable<any> {
    const url = USER_SAVE;
    return Observable.create(observer => {
      this.http.post(url, toSaveUserdata, this.fs.getHttpOptions())
        .subscribe(contentData => {
          observer.next(contentData);
          observer.complete();
        });
    });
  }
  searchUser(postData): Observable<any> {
    const encodeUrl = encodeURIComponent(postData);
    const url = USER_SEARCH + postData;
    return Observable.create(observer => {
      this.http.get<any>(url, this.fs.getHttpOptions())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  searchAllUser(): Observable<any> {
    const url = USER_SEARCH_ALL;
    return Observable.create(observer => {
      this.http.get<any>(url, this.fs.getHttpOptions())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  saveUserLogin(postData): Observable<any> {
    const url = SAVE_USER_LOGIN;
    return Observable.create(observer => {
    this.http.post(url, postData, {
      withCredentials: true,
    }).subscribe(data => {
      observer.next(data);
      observer.complete();
    });
    });
  }

  fetchAllRoles(): Observable<any> {
    let rolesMap = new Map<String, String>();
    const data = [];
    const rolesList = [];
    const RolesMapName = new Map<String, String>();
    return Observable.create(observer => {
      this.pick.fetchPickListValues().subscribe(dataReturned => {
        const loc = <Map<String, Map<String, String>>>dataReturned;
        rolesMap = loc.get('ROLES');
        rolesMap.forEach((value: string, key: string) => {
          const val: { id: String, itemName: string } = { id: '1', itemName: 'name' };
          val.id = key;
          val.itemName = value;
          rolesList.push(val);
          RolesMapName.set(key, value);
        });
        data['rolesList'] = rolesList;
        data['RolesMapName'] = RolesMapName;
        observer.next(data);
        observer.complete();
      });
    });
  }
}
