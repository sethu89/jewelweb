// All service end points go here
import {SERVER, SERVER_COMMON} from '../../server';


export const SERVICE_CONTEXT = SERVER + '/jwlsvcs/';
export const COMMON_CONTEXT = SERVER_COMMON + '/qqscommon/';


export const PICK_LIST = SERVICE_CONTEXT + 'support/picklist';

// LOGIN
export const USER_LOGIN = COMMON_CONTEXT + 'access/login';

// Dashboard
export const DASHBOARD_DATA = SERVICE_CONTEXT + 'support/dashboardData';

export const DESK_SEARCH_BYID = SERVICE_CONTEXT + 'desk/search/byId?id=';
export const DESK_SEARCH_ALL = SERVICE_CONTEXT + 'desk/form/searchDesk?';
export const DESK_SAVE = SERVICE_CONTEXT + 'desk/save';

// PRODUCT MASTER
export const PRODUCT_MASTER_SEARCH_BYID = SERVICE_CONTEXT + 'productmaster/productMaster/byId?id=';
export const PRODUCT_MASTER_SEARCH_ALL = SERVICE_CONTEXT + 'productmaster/form/searchProductMaster?';
export const PRODUCT_MASTER_SAVE = SERVICE_CONTEXT + 'productmaster/save';
export const PROP_ATTRIBUTES_BY_PRO_MASTER_ID = SERVICE_CONTEXT + 'productmaster/allPropAttributes/byProductMasterId?productMasterId=';
export const PRODUCT_MASTER_LIST = SERVICE_CONTEXT + 'productmaster/allProductMaster';

// support
export const UPLOAD_FILE = SERVICE_CONTEXT + 'support/upload';
export const DOWNLOAD_FILE = SERVICE_CONTEXT + 'support/downloadFile';


export const FIND_BARCODE = SERVICE_CONTEXT + 'product/form/searchBarcode?id=';
export const SEARCH_PRODUCT = SERVICE_CONTEXT + 'product/form/searchProduct?';
export const SEARCH_PRODUCT_LIST = SERVICE_CONTEXT + 'product/searchProductbymasterId?id=';
export const SAVE_PRODUCT = SERVICE_CONTEXT + 'product/save';

// Codes Association
export const MASTERCATEGORY_SEARCH = SERVICE_CONTEXT + 'codes/form/searchCodeAssoc?';
export const MASTERCATEGORY_SEARCH_BYID = SERVICE_CONTEXT + 'codes/searchCodeAssoc/byId?id=';
export const MASTERCATEGORY_SAVE = SERVICE_CONTEXT + 'codes/saveCodeAssoc';
export const PICKLIST_REFRESH = SERVICE_CONTEXT + 'support/picklist/refresh';


// codes
export const MASTERDATA_SEARCH = SERVICE_CONTEXT + 'codes/form/search?category=';
export const MASTERDATA_SEARCH_BYID = SERVICE_CONTEXT + 'codes/search/byId?id=';
export const MASTERDATA_SAVE = SERVICE_CONTEXT + 'codes/save';


// Report
export const INVENTORY_TRANSACTION_REPORT = SERVICE_CONTEXT + 'report/jobTransactionReport?';
export const PRODUCT_MASTER_REPORT = SERVICE_CONTEXT + 'report/productMasterReport?';
export const PRODUCT_DETAIL_REPORT = SERVICE_CONTEXT + 'report/productDetailReport?';
export const DESK_REPORT = SERVICE_CONTEXT + 'report/deskReport?';


//Transactions
export const JOB_TRANSACTION_DATA = SERVICE_CONTEXT + 'jobtrans/jobTransactionData?';


// for user
export const USER_SEARCH_ALL = COMMON_CONTEXT + 'access/getUserName';
export const USER_DETAIl = COMMON_CONTEXT + 'access/getUserDetail/byUserName?userName=';
export const USER_SAVE = COMMON_CONTEXT + 'access/saveUser';
export const USER_SEARCH = COMMON_CONTEXT + 'access/searchUser?';
export const USER_SEARCH_BYID = COMMON_CONTEXT + 'access/searchUser/byId?id=';


// reset password
export const RESET_PASSWORD = COMMON_CONTEXT + 'access/password/reset';

// Save Loginn
export const SAVE_USER_LOGIN = SERVICE_CONTEXT + 'support/saveUserLogin';


export const GENERATE_BARCODE = SERVICE_CONTEXT + 'support/generateBarcode';

// JOBTRANSACTION
export const JOBTRANSACTION_SEARCH_BYID = SERVICE_CONTEXT + 'jobtrans/jobTransaction/byId?id=';
export const JOBTRANSACTION_SEARCH_ALL = SERVICE_CONTEXT + 'jobtrans/form/searchJobTransaction?';
export const JOBTRANSACTION_SAVE = SERVICE_CONTEXT + 'jobtrans/save';


// BarCodeDetails
export const BARCODEDETAIL_BY_PRODUCTMASTERID = SERVICE_CONTEXT + 'barcodedetails/search/byproductmasterid?productMasterId=';
export const GENERATE_NO_BY_PRODUCTMASTERID = SERVICE_CONTEXT + 'barcodedetails/search/lastbarcode/byproductmasterid?productMasterId=';
export const SAVE_BARCODEDETAILS = SERVICE_CONTEXT + 'barcodedetails/save';
