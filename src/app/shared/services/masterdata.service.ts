import { MASTERDATA_SEARCH , MASTERDATA_SAVE , MASTERDATA_SEARCH_BYID } from './endpoints';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FactoryService} from './factory.service';
import {Observable} from 'rxjs';

@Injectable()
export class MasterService {
    constructor(private http: HttpClient, private fs: FactoryService) {
         }

         fetchMasterDataById(masterDataId: number): Observable<any> {
            const url = MASTERDATA_SEARCH_BYID + masterDataId;
            return Observable.create(observer => {
              this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(data => {
                  observer.next(data);
                  observer.complete();
                });
            });
          }
          saveMasterData(toSaveMasterDatadata): Observable<any> {
            const url = MASTERDATA_SAVE;
            return Observable.create(observer => {
              this.http.post(url, toSaveMasterDatadata, this.fs.getHttpOptions())
                .subscribe(contentData => {
                  observer.next(contentData);
                  observer.complete();
                });
            });
          }
          searchMasterData(postData): Observable<any> {
            const url = MASTERDATA_SEARCH + postData + '&exactMatch=true';
            return Observable.create(observer => {
              this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(data => {
                  observer.next(data);
                  observer.complete();
                });
            });
          }
}
