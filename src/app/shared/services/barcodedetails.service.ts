// @ts-ignore
import {BARCODEDETAIL_BY_PRODUCTMASTERID, DESK_SAVE, GENERATE_NO_BY_PRODUCTMASTERID, SAVE_BARCODEDETAILS} from './endpoints';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FactoryService } from '.';


@Injectable()
export class BarcodedetailsService {

    constructor(private http: HttpClient, private fs: FactoryService, ) {
    }

    fetchBarCodeDetailsListByProductMasterId(productMasterId: number): Observable<any> {
        const url = BARCODEDETAIL_BY_PRODUCTMASTERID + productMasterId;
        return Observable.create(observer => {
            this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(data => {
                    observer.next(data);
                    observer.complete();
                });
        });
    }

    fetchLastGeneratedBarcodeByProductMasterId(productMasterId: number): Observable<any> {
        const url = GENERATE_NO_BY_PRODUCTMASTERID + productMasterId;
        return Observable.create(observer => {
            this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(data => {
                    observer.next(data);
                    observer.complete();
                });
        });
    }

    saveBarcodeDetails(toSaveBarcodeData): Observable<any> {
        const url = SAVE_BARCODEDETAILS;
        return Observable.create(observer => {
            this.http.post(url, toSaveBarcodeData, this.fs.getHttpOptions())
                .subscribe(contentData => {
                    observer.next(contentData);
                    observer.complete();
                });
        });
    }


}
