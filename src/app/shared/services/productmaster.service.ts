// @ts-ignore
import {
    DESK_SAVE,
    FIND_BARCODE,
    PRODUCT_MASTER_LIST,
    PRODUCT_MASTER_SAVE,
    PRODUCT_MASTER_SEARCH_ALL,
    PRODUCT_MASTER_SEARCH_BYID,
    PROP_ATTRIBUTES_BY_PRO_MASTER_ID, SAVE_PRODUCT, SEARCH_PRODUCT, SEARCH_PRODUCT_LIST
} from './endpoints';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { FactoryService } from './factory.service';
import { Observable } from 'rxjs';
import { FactoryService } from '.';


@Injectable()
export class ProductMasterService {

    constructor(private http: HttpClient, private fs: FactoryService, ) {
    }


    fetchAllProductList(): Observable<any> {
        const url = SEARCH_PRODUCT;
        const productMasterMap = new Map<String, String>();
        const data = [];
        const productMasterList = [];
        return Observable.create(observer => {
            this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(returnData => {
                    returnData.forEach(item => {
                        const val: { id: String, itemName: string } = { id: '1', itemName: 'name' };
                        val.id = item.id;
                        val.itemName = item.barCode;
                        productMasterList.push(val);
                        productMasterMap.set(val.id, val.itemName);
                    });
                    data['productMasterList'] = productMasterList;
                    data['productMasterMap'] = productMasterMap;
                    observer.next(data);
                    observer.complete();
                });
        });
    }

    fetchProductMasterById(productMasterId: number): Observable<any> {
        const url = PRODUCT_MASTER_SEARCH_BYID + productMasterId;
        return Observable.create(observer => {
            this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(data => {
                    observer.next(data);
                    observer.complete();
                });
        });
    }

    saveProductMaster(toSaveProductMasterData): Observable<any> {
        const url = PRODUCT_MASTER_SAVE;
        return Observable.create(observer => {
            this.http.post(url, toSaveProductMasterData, this.fs.getHttpOptions())
                .subscribe(contentData => {
                    observer.next(contentData);
                    observer.complete();
                });
        });
    }

    fetchAllProductMaster(postData): Observable<any> {
        const url = PRODUCT_MASTER_SEARCH_ALL + postData ;
        return Observable.create(observer => {
            this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(data => {
                    observer.next(data);
                    observer.complete();
                });
        });
    }

    fetchAllProductMasterData(): Observable<any> {
        const url = PRODUCT_MASTER_SEARCH_ALL ;
        return Observable.create(observer => {
            this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(data => {
                    observer.next(data);
                    observer.complete();
                });
        });
    }

    fetchPropAttributesByProductMasterId(productMasterId): Observable<any> {
        const url = PROP_ATTRIBUTES_BY_PRO_MASTER_ID + productMasterId;
        return Observable.create(observer => {
            this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(data => {
                    observer.next(data);
                    observer.complete();
                });
        });
    }

    fetchProductByProductMasterById(productMasterId: number): Observable<any> {
        const url = FIND_BARCODE + productMasterId;
        return Observable.create(observer => {
            this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(data => {
                    observer.next(data);
                    observer.complete();
                });
        });
    }
    fetchProductListByProductMasterById(productMasterId: number): Observable<any> {
        const url = SEARCH_PRODUCT_LIST + productMasterId;
        return Observable.create(observer => {
            this.http.get<any>(url, this.fs.getHttpOptions())
                .subscribe(data => {
                    observer.next(data);
                    observer.complete();
                });
        });
    }

    saveProduct(toSaveProductdata): Observable<any> {
        const url = SAVE_PRODUCT;
        return Observable.create(observer => {
            this.http.post(url, toSaveProductdata, this.fs.getHttpOptions())
                .subscribe(contentData => {
                    observer.next(contentData);
                    observer.complete();
                });
        });
    }


}
