import { Component, OnInit } from '@angular/core';
import {environment} from '../../environments/environment';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  public applicationDisplayName: string;
    constructor() {
      this.applicationDisplayName = environment.applicationDisplayName;
    }

    ngOnInit() {}
}
