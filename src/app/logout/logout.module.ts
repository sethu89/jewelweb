import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LogoutComponent} from './logout.component';
import {FormsModule} from '@angular/forms';
import {LogoutRoutingModule} from './logout-routing.module';

@NgModule({
  imports: [CommonModule, LogoutRoutingModule, FormsModule],
  declarations: [LogoutComponent]
})
export class LogoutModule { }
