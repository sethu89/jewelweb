import {Component, OnInit} from '@angular/core';
import {environment} from '../../environments/environment';
import {Router} from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {
  applicationDisplayName: string;

  constructor(public router: Router) {
      this.applicationDisplayName = environment.applicationDisplayName;
  }

  ngOnInit() {
    localStorage.removeItem('roles');
    localStorage.removeItem('loggedUser');
    localStorage.removeItem('isLoggedin');
    this.router.navigate(['/login']);
  }


}
