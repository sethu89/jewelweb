
import {environment} from '../environments/environment';

export const SERVER = environment.SERVER;
export const SERVER_COMMON = environment.SERVER_COMMON;
