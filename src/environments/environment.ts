//
// // This file can be replaced during build by using the `fileReplacements` array.
// // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// // The list of file replacements can be found in `angular.json`.
//
// export const environment = {
//   production: false,
//   applicationDisplayName: 'KISNA Inventory',
//   miniName: 'KISNA',
//   SERVER: 'http://192.168.1.3:8082',
//   SERVER_QQSVS: 'http://192.168.1.3:8081',
//   SERVER_COMMON: 'http://192.168.1.3:8080',
//
//   imgFolder: 'assets/images/qqs/',
//   backgroundImg: 'assets/images/qqs/Logo.png'
// };


export const environment = {
  production: false,
  applicationDisplayName: 'KISNA Inventory',
  miniName: 'KISNA',
  SERVER: 'http://localhost:8082',
  SERVER_COMMON: 'http://localhost:8080',
  // SERVER: 'http://ec2-18-218-50-243.us-east-2.compute.amazonaws.com:8082',
  // SERVER_COMMON: 'http://ec2-18-218-50-243.us-east-2.compute.amazonaws.com:8082',

  imgFolder: 'assets/images/qqs/',
  backgroundImg: 'assets/images/qqs/logoFade.png'
};

