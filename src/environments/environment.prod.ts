
export const environment = {
  production: false,
  applicationDisplayName: 'KISNA Inventory',
  miniName: 'KISNA',
  SERVER: 'http://localhost:8080',
  SERVER_COMMON: 'http://localhost:8080',
  imgFolder: 'assets/images/qqs/',
  backgroundImg: 'assets/images/qqs/logoFade.png'
};

